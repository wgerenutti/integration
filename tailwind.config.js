/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,php}"],
  theme: {
      extend: {
          content: {
              "gubee-logo-black":
                  'url("/src/view/adminhtml/web/css/img/logo-black.svg")',
              "gubee-logo-white":
                  'url("/src/view/adminhtml/web/css/img/logo-white.svg")',
              "gubee-logo": 'url("/src/view/adminhtml/web/css/img/logo.svg")',
          },
      },
  },
  plugins: [
    require('tailwind-fontawesome')({
      version: 6
  })
  ],
};
