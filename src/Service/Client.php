<?php

declare(strict_types=1);

namespace Gubee\Integration\Service;

use BadMethodCallException;
use DateTime;
use DateTimeZone;
use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Log;
use Gubee\SDK\Api\TokenApi;
use Gubee\SDK\Gubee;
use Throwable;

use function sprintf;

class Client extends Gubee
{
    protected $config;
    protected $logger;

    public function __construct(
        Log $logger,
        Config $config
    ) {
        $this->logger = $logger;
        $this->config = $config;
        parent::__construct(
            $logger,
            $this->getConfig()->getMaxBackoffAttempts() ?: 3
        );
        $this->_authenticate();
    }

    protected function _authenticate()
    {
        if (! $this->getConfig()->getApiKey()) {
            throw new BadMethodCallException(
                'API key is not set'
            );
        }
        if ($this->isTokenTimeouted()) {
            try {
                $this->renewToken();
            } catch (Throwable $e) {
                $this->getLogger()->error(
                    sprintf(
                        "Error renewing API token: %s",
                        $e->getMessage()
                    )
                );
                throw $e;
            }
        }
        parent::authenticate(
            $this->getConfig()->getApiToken()
        );
    }

    public function renewToken(): void
    {
        $this->getLogger()->info('Renewing API token');
        try {
            $token = new TokenApi($this);
            $token = $token->renewApiToken(
                $this->getConfig()->getApiKey()
            );
            $this->getConfig()->setApiToken(
                $token->getToken()
            );
            $this->getConfig()->setApiTimeout(
                $token->getValidity()->format('Y-m-d\\TH:i:s.u')
            );
            $this->getConfig()->save();
            $this->getLogger()->info(
                sprintf(
                    "API token renewed, timeout: %s",
                    $token->getValidity()->format('Y-m-d\\TH:i:s.u')
                )
            );
        } catch (Throwable $e) {
            $this->getLogger()->error(
                sprintf(
                    "Error renewing API token: %s",
                    $e->getMessage()
                )
            );
            throw $e;
        }
    }

    /**
     * @return mixed
     */
    public function isTokenTimeouted(): bool
    {
        if (! $this->getConfig()->getApiTimeout()) {
            return true;
        }
        $now     = new DateTime();
        $timeout = DateTime::createFromFormat(
            'Y-m-d\\TH:i:s.u',
            $this->getConfig()->getApiTimeout(),
            new DateTimeZone('UTC')
        );
        return $now > $timeout ? true : false;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig($config): self
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param mixed $logger
     */
    public function setLogger($logger): self
    {
        $this->logger = $logger;
        return $this;
    }
}
