<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Model\Product;

use Gubee\Integration\Service\Hydration\HydrationTrait;
use Gubee\SDK\Api\Product\AttributeApi;
use Gubee\SDK\DataObject\Product\Attribute as AttributeDataObject;
use Magento\Catalog\Api\Data\EavAttributeInterface;
use Magento\Catalog\Api\ProductAttributeRepositoryInterfaceFactory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\App\ObjectManager;
use Magento\Store\Model\StoreManagerInterface;
use Throwable;

use function in_array;
use function trim;

class Attribute extends AttributeDataObject
{
    use HydrationTrait;

    protected $attributeApi;

    protected $attribute;

    protected $attributeRepositoryFactory;

    public function __construct(
        EavAttributeInterface $attribute,
        ProductAttributeRepositoryInterfaceFactory $attributeRepositoryFactory,
        AttributeApi $attributeApi
    ) {
        $this->attribute                  = $attribute;
        $this->attributeRepositoryFactory = $attributeRepositoryFactory;
        $this->attributeApi               = $attributeApi;
        $this->hydrate();
    }

    public function save()
    {
        try {
            $this->getAttributeApi()->getByExternalId($this->getId());
            $this->getAttributeApi()->updateByExternalId(
                $this->getId(),
                $this
            );
        } catch (Throwable $e) {
            $this->getAttributeApi()->create(
                $this
            );
        } finally {
            return $this;
        }
    }

    public function hydrate()
    {
        switch ($this->getAttribute()->getFrontendInput()) {
            case 'text':
            case 'price':
            default:
                $this->setAttrType(AttributeDataObject::TEXT);
                break;
            case 'textarea':
                $this->setAttrType(AttributeDataObject::TEXTAREA);
                break;
            case 'multiselect':
                $this->setAttrType(AttributeDataObject::MULTISELECT);
                break;
            case 'select':
                $this->setAttrType(AttributeDataObject::SELECT);
                break;
        }
        $this->setId(
            $this->getAttribute()->getAttributeCode()
        );
        $this->setName(
            $this->getAttribute()->getName()
        );
        $this->setLabel(
            $this->getAttribute()->getFrontendLabel()
        );
        $this->getAttribute()->setStoreId(
            ObjectManager::getInstance()->get(
                StoreManagerInterface::class
            )->getDefaultStoreView()->getId()
        );
        $options = [];
        foreach ($this->getAttribute()->getOptions() ?: [] as $option) {
            $label = (string) $option->getLabel();
            $label = trim($label);
            if ($label == '') {
                continue;
            }
            $options[] = $label;
        }
        $this->setOptions($options);
        $this->setRequired($this->getAttribute()->getIsRequired() ? true : false);
        $this->setVariant(
            self::isVariant(
                $this->getAttribute()->getAttributeCode()
            )
        );
    }

    public static function isVariant($attributeCode)
    {
        $productAttributeRepository = ObjectManager::getInstance()->create(
            ProductAttributeRepositoryInterfaceFactory::class
        )->create();
        $productAttribute           = $productAttributeRepository->get(
            $attributeCode
        );
        $result                     = false;
        if (
            $productAttribute->getIsGlobal() == 1
            && $productAttribute->getIsUserDefined() == 1
            && $productAttribute->getFrontendInput() == "select"
        ) {
            if (
                ! $productAttribute->getApplyTo()
                ||
                in_array(
                    Configurable::TYPE_CODE,
                    $productAttribute->getApplyTo()
                )
            ) {
                $result = true;
            }
        }
        return $result ? true : false;
    }

    /**
     * @return AttributeApi
     */
    public function getAttributeApi()
    {
        return $this->attributeApi;
    }

    /**
     * @return EavAttributeInterface
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * @return mixed
     */
    public function getAttributeRepositoryFactory()
    {
        return $this->attributeRepositoryFactory;
    }

    /**
     * @param mixed $attributeRepositoryFactory
     */
    public function setAttributeRepositoryFactory($attributeRepositoryFactory): self
    {
        $this->attributeRepositoryFactory = $attributeRepositoryFactory;
        return $this;
    }
}
