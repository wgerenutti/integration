<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Model\Product;

use Exception;
use Gubee\Integration\Helper\Config;
use Gubee\Integration\Service\Model\Product;
use Gubee\SDK\Api\Product\StockApi;
use Gubee\SDK\DataObject\Product\Attribute\Dimension\UnitTime;
use Gubee\SDK\DataObject\Product\Stock as StockDataObject;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\Framework\App\ObjectManager;

class Stock extends StockDataObject
{
    protected $stockApi;

    protected $product;
    protected $stockRegistry;

    public function __construct(
        ProductInterface $product,
        StockApi $stockApi,
        Config $config,
        StockRegistryInterface $stockRegistry
    ) {
        $this->config        = $config;
        $this->stockApi      = $stockApi;
        $this->product       = $product;
        $this->stockRegistry = $stockRegistry;
        $stockItem           = $this->stockRegistry->getStockItem(
            $this->product->getId()
        );

        $this->setQty(
            (int) $stockItem->getQty()
        );
        $crossDockingTime = new UnitTime();
        $crossDockingTime->setType(
            $this->getRawAttributeValue('gubee_cross_docking_time_unit') ?: UnitTime::DAYS
        );
        $crossDockingTime->setValue(
            (int) $this->getRawAttributeValue(
                $this->config->getAttributeConfig()->getCrossDockingTime()
            ) ?: -1
        );
        $this->setPriority(0);
        $this->setCrossDockingTime(
            $crossDockingTime
        );
    }

    /**
     * @return mixed
     */
    public function save()
    {
        try {
            $product = ObjectManager::getInstance()->create(
                Product::class,
                [
                    'product' => $this->getProduct(),
                ]
            );
            foreach ($product->getVariations() as $variation) {
                foreach ($variation->getStocks() as $stock) {
                    $this->getStockApi()->updateStock(
                        $product,
                        $variation,
                        $stock
                    );
                }
            }
        } catch (Exception $e) {
            throw $e;
        }

        return $this;
    }

    public function desativeStock()
    {
        try {
            $product = ObjectManager::getInstance()->create(
                Product::class,
                [
                    'product' => $this->getProduct(),
                ]
            );
            foreach ($product->getVariations() as $variation) {
                foreach ($variation->getStocks() as $stock) {
                    $stock = $stock->setQty(0);
                    $this->getStockApi()->updateStock(
                        $product,
                        $variation,
                        $stock
                    );
                }
            }
        } catch (Exception $e) {
            throw $e;
        }

        return $this;
    }

    public function getRawAttributeValue($attributeCode)
    {
        return $this->getProduct()->getResource()
            ->getAttributeRawValue(
                $this->getProduct()->getId(),
                $attributeCode,
                $this->getProduct()->getStoreId()
            );
    }

    public function getAttributeValueLabel($attributeCode)
    {
        $attribute = $this->getProduct()->getResource()
            ->getAttribute($attributeCode);
        if (! $attribute) {
            return null;
        }
        $frontend = $attribute->getFrontend();
        if (! $frontend) {
            return null;
        }
        return $frontend->getValue(
            $this->getProduct()
        );
    }

    /**
     * @return StockApi
     */
    public function getStockApi()
    {
        return $this->stockApi;
    }

    /**
     * @return mixed
     */
    public function getStockRegistry()
    {
        return $this->stockRegistry;
    }

    /**
     * @param mixed $stockRegistry
     */
    public function setStockRegistry($stockRegistry): self
    {
        $this->stockRegistry = $stockRegistry;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product): self
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @param mixed $stockApi
     */
    public function setStockApi($stockApi): self
    {
        $this->stockApi = $stockApi;
        return $this;
    }
}
