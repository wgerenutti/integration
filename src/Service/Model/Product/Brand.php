<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Model\Product;

use Exception;
use Gubee\SDK\Api\Product\BrandApi;

class Brand extends \Gubee\SDK\DataObject\Product\Brand
{
    protected $brandApi;

    public function __construct(
        BrandApi $brandApi
    ) {
        $this->brandApi = $brandApi;
    }

    public function load($id)
    {
        $brand = $this->getBrandApi()->getByExternalId($id);
        if ($brand) {
            $this->setId($brand->getId());
            $this->setName($brand->getName());
            $this->setExternalId($brand->getExternalId());
        }
        return $this;
    }

    public function loadByName($name)
    {
        try {
            $brand = $this->getBrandApi()->getByName($name);
            if ($brand) {
                $this->setId($brand->getId());
                $this->setName($brand->getName());
                $this->setExternalId($brand->getExternalId());
            }
        } catch (Exception $e) {
            return false;
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function save()
    {
        try {
            $this->getBrandApi()->getByExternalId(
                $this->getId()
            );
            $this->getBrandApi()->updateByExternalId(
                $this->getId(),
                $this
            );
        } catch (Exception $e) {
            $this->getBrandApi()->create(
                $this
            );
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getBrandApi()
    {
        return $this->brandApi;
    }

    /**
     * @param mixed $brandApi
     */
    public function setBrandApi($brandApi): self
    {
        $this->brandApi = $brandApi;
        return $this;
    }
}
