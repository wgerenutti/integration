<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Model\Product;

use Exception;
use Gubee\Integration\Helper\Config\Attribute;
use Gubee\Integration\Service\Model\Product;
use Gubee\Integration\Service\Model\Product\Variation;
use Gubee\SDK\Api\Product\PriceApi;
use Gubee\SDK\DataObject\Product\Price as PriceDataObject;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\App\ObjectManager;

class Price extends PriceDataObject
{
    protected $priceApi;
    protected $product;
    protected $attributeConfig;

    public function __construct(
        ProductInterface $product,
        PriceApi $priceApi,
        Attribute $attributeConfig
    ) {
        $this->priceApi        = $priceApi;
        $this->product         = $product;
        $this->attributeConfig = $attributeConfig;
        $this->setPrice((float) $this->getDefaultPrice());
        $this->setType(self::DEFAULT);
    }

    /**
     * @return float
     */
    public function getDefaultPrice()
    {
        $priceAttribute = $this->getAttributeConfig()->getPrice();
        return (float) $this->getProduct()->getData(
            $priceAttribute
        );
    }

    /**
     * @return mixed
     */
    public function save()
    {
        try {
            /**
             * @var Variation $variation
             */
            $product = ObjectManager::getInstance()->create(
                Product::class,
                [
                    'product' => $this->getProduct(),
                ]
            );
            foreach ($product->getVariations() as $variation) {
                if (! $variation->getSkuId()) {
                    continue;
                }
                $this->getPriceApi()->getPriceByItemId($variation->getSkuId());
                foreach ($variation->getPrices() as $price) {
                    $this->getPriceApi()->updatePrice(
                        $product,
                        $variation,
                        $price
                    );
                }
            }
        } catch (Exception $e) {
            throw $e;
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product): self
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPriceApi()
    {
        return $this->priceApi;
    }

    /**
     * @param mixed $priceApi
     */
    public function setPriceApi($priceApi): self
    {
        $this->priceApi = $priceApi;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttributeConfig()
    {
        return $this->attributeConfig;
    }

    /**
     * @param mixed $attributeConfig
     */
    public function setAttributeConfig($attributeConfig): self
    {
        $this->attributeConfig = $attributeConfig;
        return $this;
    }
}
