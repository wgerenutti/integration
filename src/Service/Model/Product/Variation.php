<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Model\Product;

use Gubee\Integration\Service\Hydration\HydrationTrait;
use Gubee\SDK\DataObject\Product\Variation as VariationDataObject;
use Magento\Framework\App\ObjectManager;
use Magento\Catalog\Api\Data\ProductInterface;

use function method_exists;

class Variation extends VariationDataObject
{
    use HydrationTrait;

    public function __construct(
        ProductInterface $product,
        ?ProductInterface $parent = null,
        array $strategies = []
    ) {
        foreach ($strategies as $key => $strategy) {
            $strategyObj = ObjectManager::getInstance()->create(
                $strategy
            );
            $strategyObj->setProduct($product);
            if (
                $parent
                &&
                method_exists($strategyObj, 'setParent')
            ) {
                $strategyObj->setParent($parent);
            }
            $this->addStrategy($key, $strategyObj);
        }
        $this->hydrate();
    }
}
