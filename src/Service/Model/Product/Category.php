<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Model\Product;

use Gubee\SDK\Api\Product\CategoryApi;
use Gubee\SDK\DataObject\Product\Category as CategoryDataObject;
use Magento\Catalog\Api\Data\CategoryInterface;

class Category extends CategoryDataObject
{
    /** @var CategoryInterface */
    protected $category;

    /** @var CategoryApi */
    protected $categoryApi;

    public function __construct(
        CategoryInterface $category,
        CategoryApi $categoryApi
    ) {
        $this->category    = $category;
        $this->categoryApi = $categoryApi;
        $this->setId($this->category->getId());
        $this->setName((string) $this->category->getName());
        $this->setDescription((string) $this->category->getDescription());
        $this->setActive((bool) $this->category->getIsActive());
        if ($category->getParentId() != 0) {
            $this->setParent(
                $category->getParentId()
            );
        }
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function getCategoryApi()
    {
        return $this->categoryApi;
    }
}
