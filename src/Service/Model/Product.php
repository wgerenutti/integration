<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Model;

use Exception;
use Gubee\Integration\Service\Hydration\HydrationTrait;
use Gubee\SDK\Api\Product\StockApi;
use Gubee\SDK\Api\ProductApi;
use Gubee\SDK\DataObject\Product as ProductDataObject;
use Gubee\SDK\DataObject\Product\Stock;
use Gubee\SDK\DataObject\Product\Variation;
use Gubee\SDK\Library\HttpClient\Exception\NotFoundException;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\App\ObjectManager;
use Throwable;

use function array_unique;

class Product extends ProductDataObject
{
    use HydrationTrait;

    protected $productApi;
    protected $product;

    public function __construct(
        ProductApi $productApi,
        ?ProductInterface $product = null,
        array $strategies = []
    ) {
        $this->productApi = $productApi;
        if ($product) {
            foreach ($strategies as $key => $strategy) {
                $strategyObj = ObjectManager::getInstance()->create(
                    $strategy
                );
                $strategyObj->setProduct($product);
                $this->addStrategy($key, $strategyObj);
            }
            $this->hydrate();
            if ($this->getType() == self::VARIANT) {
                $variantAttributes = [];
                foreach ($this->getVariations() as $variation) {
                    foreach ($variation->getVariantSpecification() as $specification) {
                        $variantAttributes[] = $specification->getAttribute();
                    }
                }
                $variantAttributes = array_unique($variantAttributes);
                $this->setVariantAttributes($variantAttributes);
            } else {
                foreach ($this->getVariations() ?: [] as $variation) {
                    $variation->setVariantSpecification([]);
                }
            }
        }
    }

    public function validateProduct()
    {
        try {
            $product = $this->getProductApi()->getById(
                $this->getId()
            );
            return $this->getProductApi()->validateUpdate(
                $this
            );
        } catch (Throwable $e) {
            return $this->getProductApi()->validateCreate(
                $this
            );
        }
    }

    public function save()
    {
        try {
            $this->getProductApi()->getById(
                $this->getId()
            );
            $this->getProductApi()->update(
                $this
            );
        } catch (NotFoundException $e) {
            $this->getProductApi()->create(
                $this
            );
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function desativate($id)
    {
        $product = $this->getProductApi()->getById($id);
        $product->setStatus(self::INACTIVE);
        $this->getProductApi()->update($product);
        $stockApi = ObjectManager::getInstance()->create(
            StockApi::class
        );
        foreach ($product->getVariations() as $variation) {
            $variation = new Variation($variation);
            foreach ($variation->getStocks() as $stock) {
                $stock = new Stock($stock);
                $stock = $stock->setQty(0);
                $stockApi->updateStock(
                    $product,
                    $variation,
                    $stock
                );
            }
        }
    }

    public function getProductApi()
    {
        return $this->productApi;
    }

    /**
     * @param mixed $productApi
     */
    public function setProductApi($productApi): self
    {
        $this->productApi = $productApi;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct($product): self
    {
        $this->product = $product;
        return $this;
    }
}
