<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration;

interface HydratorInterface
{
    public function hydrate(object $target);
}
