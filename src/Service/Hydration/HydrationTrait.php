<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration;

use Gubee\Integration\Service\Hydration\Product\GenericHydrator;

trait HydrationTrait
{
    protected $strategies = [];

    public function addStrategy(string $property, HydratorInterface $strategy)
    {
        $this->strategies[$property] = $strategy;
    }

    public function hydrate()
    {
        foreach ($this->strategies as $key => $strategy) {
            if ($strategy instanceof GenericHydrator) {
                $strategy->setField($key);
            }
            $strategy->hydrate($this);
        }
    }
}
