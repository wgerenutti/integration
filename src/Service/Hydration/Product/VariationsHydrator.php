<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product;

use Gubee\Integration\Service\Model\Product\Variation;
use Gubee\SDK\DataObject\Product;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProductType;
use Magento\Framework\App\ObjectManager;

class VariationsHydrator extends AbstractHydrator
{
    public function hydrate(object $object)
    {
        if ($this->getProduct()->getTypeId() != ConfigurableProductType::TYPE_CODE) {
            $variation = ObjectManager::getInstance()->create(
                Variation::class,
                [
                    'product' => $this->getProduct(),
                ]
            );
            $variation->setMain(true);
            return $object->setVariations([
                $variation,
            ]);
        }

        $variations = [];
        $collection = $this->getProduct()->getTypeInstance()
            ->getUsedProducts(
                $this->getProduct()
            );
        $main       = false;
        foreach ($collection as $item) {
            $variation = ObjectManager::getInstance()->create(
                Variation::class,
                [
                    'product' => $item,
                    'parent'  => $this->getProduct(),
                ]
            );
            if (! $main) {
                $main = true;
                $variation->setMain($main);
            }
            if ($variation->getStatus() != Product::ACTIVE) {
                foreach ($variation->getStocks() as $stock) {
                    $stock->setQty(0);
                }
            }

            $variations[] = $variation;
        }
        return $object->setVariations($variations);
    }
}
