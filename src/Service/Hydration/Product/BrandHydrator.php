<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product;

use Exception;
use Gubee\Integration\Service\Model\Product\Brand;
use Magento\Framework\App\ObjectManager;
use Throwable;

use function get_class;
use function sprintf;

class BrandHydrator extends AbstractHydrator
{
    public function hydrate(object $object)
    {
        $this->logHydration($object);

        $brandAttribute = $this->getAttributeConfig()->getBrand();
        $rawBrandValue  = $this->getRawAttributeValue($brandAttribute);
        $brandId        = $this->getBrandId();
        if (!$brandId && $rawBrandValue) {
            $brand = ObjectManager::getInstance()
                ->create(Brand::class);
            $brand->setId(
                (string) $rawBrandValue ?: ''
            );
            $brand->setName(
                $rawBrandValue
            );
            try {
                $brand->save();
                $brandId = $brand->loadByName($rawBrandValue)->getId();
            } catch (Exception $e) {
                throw $e;
            }
        }
        if ($brandId) {
            return $object->setBrand(
                (string) $brandId
            );
        }
    }

    private function logHydration(object $object): void
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($object),
                static::class
            )
        );
    }

    private function getBrandId(): ?string
    {
        $brandAttribute = $this->getAttributeConfig()->getBrand();
        $rawBrandValue  = $this->getRawAttributeValue($brandAttribute);

        if (empty($rawBrandValue)) {
            return null;
        }

        $brand = $this->load($rawBrandValue);
        return $brand ? $brand->getId() : '';
    }

    private function load($rawBrandValue)
    {
        try {
            // First, attempt to load by ID
            return ObjectManager::getInstance()->create(Brand::class)->load($rawBrandValue);
        } catch (Throwable $e) {
            // If failed, continue to the next step
        }

        try {
            // Next, attempt to load by Name
            $brandName = $this->getAttributeValueLabel($rawBrandValue);
            return ObjectManager::getInstance()->create(Brand::class)->loadByName($brandName);
        } catch (Throwable $e) {
            // If failed, continue to the next step
        }
    }
}
