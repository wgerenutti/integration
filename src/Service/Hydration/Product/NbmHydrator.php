<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product;

use function get_class;
use function sprintf;

class NbmHydrator extends AbstractHydrator
{
    public function hydrate(object $object)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($object),
                static::class
            )
        );

        $nbm = $this->getAttributeValueLabel(
            $this->getAttributeConfig()->getNbm()
        );
        if ($nbm) {
            return $object->setNbm(
                $nbm
            );
        }
    }
}
