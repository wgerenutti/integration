<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product\Variation;

use Gubee\Integration\Service\Hydration\Product\AbstractHydrator;
use Gubee\SDK\DataObject\Product\Attribute\Dimension\UnitTime;

use function get_class;
use function sprintf;

class HandlingTimeHydrator extends AbstractHydrator
{
    public function hydrate(object $object)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($object),
                static::class
            )
        );

        $handlingTime = $this->getRawAttributeValue(
            'gubee_handling_time'
        );
        if ($handlingTime) {
            $handlingTime = new UnitTime(
                [
                    'value' => (int) $handlingTime,
                    'unit'  => $this->getUnit(),
                ]
            );
            return $object->setHandlingTime(
                $handlingTime
            );
        }
    }

    public function getUnit()
    {
        return $this->getRawAttributeValue('gubee_handling_time_unit');
    }
}
