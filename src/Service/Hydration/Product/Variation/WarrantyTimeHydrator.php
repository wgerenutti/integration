<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product\Variation;

use Gubee\Integration\Service\Hydration\Product\AbstractHydrator;
use Gubee\SDK\DataObject\Product\Attribute\Dimension\UnitTime;

use function get_class;
use function sprintf;

class WarrantyTimeHydrator extends AbstractHydrator
{
    public function hydrate(object $object)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($object),
                static::class
            )
        );

        $warrantyTime = $this->getRawAttributeValue(
            $this->getAttributeConfig()->getWarrantyTime()
        );
        if ($warrantyTime) {
            $warrantyTime = new UnitTime(
                [
                    'value' => (int) $warrantyTime,
                    'unit'  => $this->getUnit(),
                ]
            );
            return $object->setWarrantyTime(
                $warrantyTime
            );
        }
    }

    public function getUnit()
    {
        return $this->getRawAttributeValue('gubee_warranty_time_unit');
    }
}
