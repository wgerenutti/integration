<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product\Variation;

use Gubee\Integration\Service\Hydration\Product\AbstractHydrator;
use Gubee\SDK\DataObject\Product\Price;

use function get_class;
use function sprintf;

class PricesHydrator extends AbstractHydrator
{
    public function hydrate(object $object)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($object),
                static::class
            )
        );

        $defaultPrice = new Price();
        $defaultPrice->setType(
            Price::DEFAULT
        )->setValue(
            $this->getDefaultPrice()
        );
        $object->setPrices([
            $defaultPrice,
        ]);
    }

    public function getDefaultPrice()
    {
        $priceAttribute = $this->getAttributeConfig()->getPrice();
        if ($priceAttribute == 'price') {
            return (float) $this->getProduct()->getPrice();
        }

        return (float) $this->getRawAttributeValue($priceAttribute);
    }
}
