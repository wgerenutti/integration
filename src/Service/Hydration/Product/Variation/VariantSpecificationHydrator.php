<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product\Variation;

use Gubee\Integration\Service\Hydration\Product\SpecificationsHydrator;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProductType;

class VariantSpecificationHydrator extends SpecificationsHydrator
{
    public function hydrate(object $object)
    {
        if ($this->getProduct()->getTypeId() == ConfigurableProductType::TYPE_CODE) {
            return $object->setVariantSpecification($this->getVariantAttributes());
        }

        return $object->setVariantSpecification(
            $this->handleNonConfigurableProduct()
        );
    }
}
