<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product\Variation;

use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Config\Attribute;
use Gubee\Integration\Model\ResourceModel\Attribute\Collection;
use Gubee\Integration\Service\Hydration\Product\AbstractHydrator;
use Gubee\SDK\DataObject\Product\Attribute\Value;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProductType;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;
use Psr\Log\LoggerInterface;

use function array_filter;
use function array_map;
use function array_shift;
use function get_class;
use function in_array;
use function is_array;
use function sprintf;

class SpecificationHydrator extends AbstractHydrator
{
    /** @var Configurable */
    protected $typeConfigurable;

    /** @var ProductRepositoryInterface */
    protected $productRepository;

    /** @var Collection */
    protected $collection;

    public function __construct(
        Collection $collection,
        Configurable $typeConfigurable,
        ProductRepositoryInterface $productRepository,
        ProductInterface $product,
        Config $config,
        Attribute $attributeConfig,
        LoggerInterface $logger
    ) {
        $this->typeConfigurable  = $typeConfigurable;
        $this->productRepository = $productRepository;
        $this->collection        = $collection;
        parent::__construct(
            $product,
            $config,
            $attributeConfig,
            $logger
        );
    }

    public function hydrate(object $object)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($object),
                static::class
            )
        );
        if ($this->getCalledFromVariation()) {
            $this->getLogger()->debug(
                sprintf(
                    "Product '%s' is a variation",
                    $this->getProduct()->getSku()
                )
            );
            return [];
        }

        if ($this->getProduct()->getTypeId() == Configurable::TYPE_CODE) {
            return $this->getVariantAttributes();
        }

        return $this->handleNonConfigurableProduct();
    }

    private function handleNonConfigurableProduct()
    {
        $parents = $this->getTypeConfigurable()
            ->getParentIdsByChild(
                $this->getProduct()->getId()
            );

        if (empty($parents)) {
            return [];
        }

        $parent = array_shift($parents);
        return $this->getSuperAttributeData($parent);
    }

    /**
     * @param  $productId
     * @return mixed
     */
    public function getSuperAttributeData($productId)
    {
        /**
         * @var Product $product
         */
        $product = $this->getProductRepository()->getById($productId);
        if ($product->getTypeId() != ConfigurableProductType::TYPE_CODE) {
            return [];
        }
        $productTypeInstance = $product->getTypeInstance();
        $productTypeInstance->setStoreFilter($product->getStoreId(), $product);

        $attributes         = $productTypeInstance->getConfigurableAttributes($product);
        $superAttributeList = [];
        foreach ($attributes as $attribute) {
            $attributeCode = $attribute->getProductAttribute()->getAttributeCode();
            if (! $attributeCode) {
                continue;
            }
            $superAttributeList[] = $this->createAttributeValue(
                $attributeCode
            );
        }
        return $superAttributeList;
    }

    private function getVariantAttributes()
    {
        $attributes        = $this->getCollection()
            ->addFieldToFilter('is_visible_on_front', 1);
        $variantAttributes = [];

        foreach ($attributes as $attribute) {
            if (
                in_array(
                    $attribute->getAttributeCode(),
                    $this->getAttributeConfig()->getBlacklist()
                )
            ) {
                continue;
            }
            $attributeCode = $attribute->getAttributeCode();
            $productData   = $this->getProduct()->getData($attributeCode);

            if (! $attributeCode || ! $productData) {
                continue;
            }

            $attributeValue      = $this->createAttributeValue($attributeCode, $productData);
            $variantAttributes[] = $attributeValue;
        }

        return $variantAttributes;
    }

    private function createAttributeValue($attributeCode)
    {
        $attributeValue = new Value();
        $attributeValue->setAttribute($attributeCode);

        $values = $this->getAttributeValueLabel($attributeCode);
        $values = is_array($values) ? $values : [$values];
        $values = array_filter(array_map('trim', $values));
        $attributeValue->setValues($values);
        return $attributeValue;
    }

    /**
     * @return ProductRepositoryInterface
     */
    public function getProductRepository()
    {
        return $this->productRepository;
    }

    /**
     * @param ProductRepositoryInterface $productRepository
     */
    public function setProductRepository($productRepository): self
    {
        $this->productRepository = $productRepository;
        return $this;
    }

    /**
     * @return Configurable
     */
    public function getTypeConfigurable()
    {
        return $this->typeConfigurable;
    }

    /**
     * @param Configurable $typeConfigurable
     */
    public function setTypeConfigurable($typeConfigurable): self
    {
        $this->typeConfigurable = $typeConfigurable;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        return $this->collection;
    }

    /**
     * @param Collection $collection
     */
    public function setCollection($collection): self
    {
        $this->collection = $collection;
        return $this;
    }
}
