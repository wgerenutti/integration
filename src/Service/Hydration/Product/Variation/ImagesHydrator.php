<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product\Variation;

use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Config\Attribute;
use Gubee\Integration\Service\Hydration\Product\AbstractHydrator;
use Gubee\SDK\DataObject\Product\Media\Image;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product\Gallery\ReadHandler as GalleryReadHandler;
use Magento\Framework\App\ObjectManager;
use Psr\Log\LoggerInterface;

use function get_class;
use function pathinfo;
use function sprintf;

use const PATHINFO_FILENAME;

class ImagesHydrator extends AbstractHydrator
{
    /** @var GalleryReadHandler */
    protected $galleryReadHandler;

    public function __construct(
        GalleryReadHandler $galleryReadHandler,
        ProductInterface $product,
        Config $config,
        Attribute $attributeConfig,
        LoggerInterface $logger
    ) {
        $this->galleryReadHandler = $galleryReadHandler;
        parent::__construct(
            $product,
            $config,
            $attributeConfig,
            $logger
        );
    }

    public function hydrate(object $target)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($target),
                static::class
            )
        );
        return $target->setImages(
            $this->getImages()
        );
    }

    private function getImages(): array
    {
        $images             = [];
        $galleryReadHandler = $this->getGalleryReadHandler();
        $galleryReadHandler->execute($this->getProduct());
        $mediaGalleryEntries = $this->getProduct()->getMediaGalleryImages();
        if (empty($mediaGalleryEntries)) {
            return $this->getPlaceholder();
        }
        foreach ($mediaGalleryEntries as $mediaGalleryEntry) {
            $images[] = $this->createImage($mediaGalleryEntry);
        }

        return $images;
    }

    private function createImage($mediaGalleryEntry): Image
    {

        if (!$url = $mediaGalleryEntry->getUrl()) {
            $url = sprintf(
                "%scatalog%s",
                ObjectManager::getInstance()->get(
                    \Magento\Store\Model\StoreManagerInterface::class
                )->getStore()->getBaseUrl(
                    \Magento\Framework\UrlInterface::URL_TYPE_MEDIA
                ),
                $mediaGalleryEntry->getFile()
            );
        }

        $url = str_replace('http://', 'https://', $url);
        $image = new Image();
        $image->setName(
            $mediaGalleryEntry->getLabel() ?: pathinfo(
                $mediaGalleryEntry->getFile(),
                PATHINFO_FILENAME
            )
        )->setUrl(
            $url
        )->setOrder(
            $mediaGalleryEntry->getPosition() ? (int) $mediaGalleryEntry->getPosition() : 0
        )->setMain(
                // check if is thumbnail
            $mediaGalleryEntry->getFile() === $this->getProduct()->getThumbnail()
        );
        return $image;
    }

    /**
     * @return mixed
     */
    protected function getPlaceholder(): Image
    {
        $image = new Image();
        $image->setName("Placeholder")
            ->setUrl($this->getPlaceholderUrl())
            ->setOrder(1)
            ->setMain(true);
        return $image;
    }

    protected function getPlaceholderUrl(): string
    {
        return "https://placehold.co/600";
    }

    /**
     * @return GalleryReadHandler
     */
    public function getGalleryReadHandler()
    {
        return $this->galleryReadHandler;
    }

    /**
     * @param GalleryReadHandler $galleryReadHandler
     */
    public function setGalleryReadHandler($galleryReadHandler): self
    {
        $this->galleryReadHandler = $galleryReadHandler;
        return $this;
    }
}
