<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product\Variation;

use Gubee\Integration\Service\Hydration\Product\AbstractHydrator;
use Gubee\SDK\DataObject\Product\Attribute\Dimension;
use Gubee\SDK\DataObject\Product\Attribute\Dimension\Measure;
use Gubee\SDK\DataObject\Product\Attribute\Dimension\Weight;

use function get_class;
use function print_r;
use function sprintf;

class DimensionHydrator extends AbstractHydrator
{
    public function hydrate(object $target)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($target),
                static::class
            )
        );
        return $target->setDimension(
            $this->getDimension()
        );
    }

    private function getDimension(): Dimension
    {
        $this->getLogger()->debug(
            sprintf(
                "Dimension attributes: '%s'",
                print_r([
                    'depth'  => $this->getAttributeConfig()->getDepth(),
                    'height' => $this->getAttributeConfig()->getHeight(),
                    'width'  => $this->getAttributeConfig()->getWidth(),
                    'weight' => 'weight',
                ], true)
            )
        );
        $dimension = new Dimension();
        $dimension->setDepth(
            $this->createMeasure(
                $this->getAttributeConfig()->getDepth()
            )
        )->setHeight(
            $this->createMeasure(
                $this->getAttributeConfig()->getHeight()
            )
        )->setWeight(
            $this->createWeight()
        )->setWidth(
            $this->createMeasure(
                $this->getAttributeConfig()->getWidth()
            )
        );
        $this->getLogger()->debug(
            sprintf(
                "Dimension: '%s'",
                print_r($dimension, true)
            )
        );
        return $dimension;
    }

    private function createMeasure(?string $attribute)
    {
        $measure = new Measure();
        $measure->setType(
            $this->getAttributeConfig()->getMeasureUnit()
        );
        $measure->setValue(
            (float) $this->getRawAttributeValue($attribute)
        );

        return $measure;
    }

    private function createWeight(): Weight
    {
        $weight = new Weight();
        $weight->setType(
            $this->getAttributeConfig()->getWeightUnit()
        );
        $weight->setValue(
            (float) $this->getRawAttributeValue('weight')
        );

        return $weight;
    }
}
