<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product\Variation;

use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Config\Attribute;
use Gubee\Integration\Service\Hydration\Product\AbstractHydrator;
use Gubee\SDK\DataObject\Product\Attribute\Dimension\UnitTime;
use Gubee\SDK\DataObject\Product\Stock;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Psr\Log\LoggerInterface;

use function get_class;
use function sprintf;

class StocksHydrator extends AbstractHydrator
{
    /** @var StockRegistryInterface */
    protected $stockRegistry;

    public function __construct(
        StockRegistryInterface $stockRegistry,
        ProductInterface $product,
        Config $config,
        Attribute $attributeConfig,
        LoggerInterface $logger
    ) {
        $this->stockRegistry = $stockRegistry;
        parent::__construct(
            $product,
            $config,
            $attributeConfig,
            $logger
        );
    }

    public function hydrate(object $target)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($target),
                static::class
            )
        );
        $stockItem = $this->getStockRegistry()
            ->getStockItem(
                $this->getProduct()->getId()
            );

        $stock = new Stock();
        $stock->setQty(
            (int) $stockItem->getQty()
        );
        $crossDockingTime = new UnitTime();
        $crossDockingTime->setType(
            $this->getRawAttributeValue('gubee_cross_docking_time_unit') ?: UnitTime::DAYS
        );
        $crossDockingTime->setValue(
            (int) $this->getRawAttributeValue(
                $this->config->getAttributeConfig()->getCrossDockingTime()
            ) !== null ? (int) $this->getRawAttributeValue(
                $this->config->getAttributeConfig()->getCrossDockingTime()
            ) : -1
        );
        $stock->setPriority(0);
        $stock->setCrossDockingTime(
            $crossDockingTime
        );
        return $target->setStocks([$stock]);
    }

    /**
     * @return StockRegistryInterface
     */
    public function getStockRegistry()
    {
        return $this->stockRegistry;
    }

    /**
     * @param StockRegistryInterface $stockRegistry
     */
    public function setStockRegistry($stockRegistry): self
    {
        $this->stockRegistry = $stockRegistry;
        return $this;
    }
}
