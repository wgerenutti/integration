<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product\Variation;

use Gubee\Integration\Service\Hydration\Product\AbstractHydrator;

use function get_class;
use function sprintf;

class CostHydrator extends AbstractHydrator
{
    public function hydrate(object $target)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($target),
                static::class
            )
        );
        return $target->setCost(
            (float) $this->getRawAttributeValue('cost') ?: 0
        );
    }
}
