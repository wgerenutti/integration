<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product\Variation;

use Gubee\Integration\Service\Hydration\Product\AbstractHydrator;

use function get_class;
use function is_array;
use function is_string;
use function sprintf;

class EanHydrator extends AbstractHydrator
{
    public function hydrate(object $object)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($object),
                static::class
            )
        );

        $ean = $this->getAttributeValueLabel(
            $this->getAttributeConfig()->getEan()
        );
        if ($ean) {
            return $object->setEan(
                (string) $ean
            );
        }
        $value = $this->getRawAttributeValue(
            $this->getAttributeConfig()->getEan()
        );
        if (is_array($value)) {
            $value = '';
        }

        return $object->setEan(
            is_string($value) ? $value : (string) $value
        );
    }
}
