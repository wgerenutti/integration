<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product\Variation;

use Gubee\Integration\Service\Hydration\Product\AbstractHydrator;

use function get_class;
use function sprintf;

class SkuHydrator extends AbstractHydrator
{
    protected $parent;

    public function hydrate(object $object)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($object),
                static::class
            )
        );
        return $object->setSku(
            $this->getParent() ?
            sprintf(
                "%s-%s",
                $this->getParent() ? $this->getParent()->getSku() : $this->getProduct()->getSku(),
                $this->getProduct()->getSku()
            ) : $this->getProduct()->getSku()
        );
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent): self
    {
        $this->parent = $parent;
        return $this;
    }
}
