<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product;

use Gubee\Integration\Model\System\Config\Source\MainCategory;

use function get_class;
use function sprintf;

class MainCategoryHydrator extends AbstractHydrator
{
    public function hydrate(object $object)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($object),
                static::class
            )
        );
        if ($this->getAttributeConfig()->getMainCategory() == MainCategory::DEEPER) {
            $item = $this->getCategoryCollection()
                ->getLastItem()
                ->getId();
            $this->getLogger()->debug(
                sprintf(
                    "Product '%s' is the deepest category for '%s' product",
                    $item,
                    $this->getProduct()->getSku()
                )
            );
            if ($item) {
                return $object->setMainCategory(
                    $item
                );
            }
            return $this;
        }
        $item = $this->getCategoryCollection()
            ->getFirstItem()
            ->getId();
        $this->getLogger()->debug(
            sprintf(
                "Product '%s' is the highest category for '%s' product",
                $item,
                $this->getProduct()->getSku()
            )
        );
        return $object->setMainCategory(
            $item
        );
    }

    public function getCategoryCollection()
    {
        return $this->getProduct()
        ->getCategoryCollection()
        ->addAttributeToSelect('name')
        ->addFieldToSelect('name')
        ->addAttributeToSort('level', 'DESC')
        ->load();
    }
}
