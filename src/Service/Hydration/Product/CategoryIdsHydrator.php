<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product;

class CategoryIdsHydrator extends AbstractHydrator
{
    public function hydrate(object $target)
    {
        $attributeIds   = $this->getProduct()->getCategoryIds();
        $rootCategoryId = $this->getProduct()->getStore()->getRootCategoryId();

        $categories = $this->getProduct()->getCategoryCollection()
            ->addAttributeToFilter('entity_id', ['in' => $attributeIds])
            ->addAttributeToFilter('entity_id', ['neq' => $rootCategoryId])
            ->addAttributeToSort('level', 'ASC');

        $ids = [];
        foreach ($categories as $category) {
            $ids[] = $category->getId();
        }
        return $target->setCategories(
            $ids
        );
    }
}
