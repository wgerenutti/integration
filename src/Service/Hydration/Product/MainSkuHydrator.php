<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product;

use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Config\Attribute;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product\Type;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;
use Psr\Log\LoggerInterface;

use function count;
use function get_class;
use function sprintf;

class MainSkuHydrator extends AbstractHydrator
{
    /** @var Configurable */
    protected $typeConfigurable;

    public function __construct(
        Configurable $typeConfigurable,
        ProductInterface $product,
        Config $config,
        Attribute $attributeConfig,
        LoggerInterface $logger
    ) {
        $this->typeConfigurable = $typeConfigurable;
        parent::__construct(
            $product,
            $config,
            $attributeConfig,
            $logger
        );
    }

    public function hydrate(object $object)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($object),
                static::class
            )
        );

        if ($this->getProduct()->getTypeId() != Type::TYPE_SIMPLE) {
            $this->getLogger()->debug(
                sprintf(
                    "Product '%s' is not a simple product",
                    $this->getProduct()->getSku()
                )
            );
            return $object->setMainSku(
                $this->getProduct()->getSku()
            );
        }

        $parents = $this->getTypeConfigurable()
            ->getParentIdsByChild(
                $this->getProduct()->getId()
            );
        if (count($parents) > 0) {
            $this->getLogger()->debug(
                sprintf(
                    "Product '%s' is a variant product base on number of parents",
                    $this->getProduct()->getSku()
                )
            );
            return $object->setMainSku(
                $this->getProduct()->getSku()
            );
        }

        $this->getLogger()->debug(
            sprintf(
                "Product '%s' is a simple product",
                $this->getProduct()->getSku()
            )
        );
        return $object->setMainSku(
            $this->getProduct()->getSku()
        );
    }

    /**
     * @return Configurable
     */
    public function getTypeConfigurable()
    {
        return $this->typeConfigurable;
    }

    /**
     * @param Configurable $typeConfigurable
     */
    public function setTypeConfigurable($typeConfigurable): self
    {
        $this->typeConfigurable = $typeConfigurable;
        return $this;
    }
}
