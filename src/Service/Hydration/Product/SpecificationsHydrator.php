<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product;

use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Config\Attribute;
use Gubee\Integration\Model\ResourceModel\Attribute\Collection;
use Gubee\SDK\DataObject\Product\Attribute\Value;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProductType;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;
use Psr\Log\LoggerInterface;

use function array_filter;
use function array_map;
use function array_shift;
use function in_array;
use function is_array;

class SpecificationsHydrator extends AbstractHydrator
{
    /** @var Configurable */
    protected $typeConfigurable;

    /** @var Collection */
    protected $collection;

    /** @var ProductRepositoryInterface */
    protected $productRepository;

    public function __construct(
        Configurable $typeConfigurable,
        Collection $collection,
        ProductRepositoryInterface $productRepository,
        ProductInterface $product,
        Config $config,
        Attribute $attributeConfig,
        LoggerInterface $logger
    ) {
        $this->typeConfigurable  = $typeConfigurable;
        $this->collection        = $collection;
        $this->productRepository = $productRepository;
        parent::__construct(
            $product,
            $config,
            $attributeConfig,
            $logger
        );
    }

    public function hydrate(object $object)
    {
        return $object->setSpecifications($this->getVariantAttributes());
    }

    /**
     * @return mixed
     */
    protected function handleNonConfigurableProduct()
    {
        $parents = $this->getTypeConfigurable()
            ->getParentIdsByChild(
                $this->getProduct()->getId()
            );

        if (empty($parents)) {
            return [];
        }

        $parent = array_shift($parents);
        return $this->getSuperAttributeData($parent);
    }

    /**
     * @param  $productId
     * @return mixed
     */
    public function getSuperAttributeData($productId)
    {
        /**
         * @var Product $product
         */
        $product = $this->getProductRepository()->getById($productId);
        if ($product->getTypeId() != ConfigurableProductType::TYPE_CODE) {
            return [];
        }
        $productTypeInstance = $product->getTypeInstance();
        $productTypeInstance->setStoreFilter($product->getStoreId(), $product);

        $attributes         = $productTypeInstance->getConfigurableAttributes($product);
        $superAttributeList = [];
        foreach ($attributes as $attribute) {
            if (
                in_array(
                    $attribute->getAttributeCode(),
                    $this->getAttributeConfig()->getBlacklist()
                )
            ) {
                continue;
            }
            $attributeCode = $attribute->getProductAttribute()->getAttributeCode();
            if (! $attributeCode) {
                continue;
            }
            $superAttributeList[] = $this->createAttributeValue(
                $attributeCode
            );
        }
        return $superAttributeList;
    }

    protected function getVariantAttributes()
    {
        $attributes        = $this->getCollection();
        $variantAttributes = [];

        foreach ($attributes as $attribute) {
            if (
                in_array(
                    $attribute->getAttributeCode(),
                    $this->getAttributeConfig()->getBlacklist()
                )
            ) {
                continue;
            }
            $attributeCode = $attribute->getAttributeCode();
            $productData   = $this->getProduct()->getData($attributeCode)
                ?: $this->getRawAttributeValue($attributeCode);
            if (! $attributeCode || ! $productData) {
                continue;
            }

            $attributeValue = $this->createAttributeValue($attributeCode, $productData);
            if (! $attributeValue->getValues()) {
                continue;
            }
            $variantAttributes[] = $attributeValue;
        }

        return $variantAttributes;
    }

    /**
     * @param  $attributeCode
     * @return mixed
     */
    private function createAttributeValue($attributeCode)
    {
        $attributeValue = new Value();
        $attributeValue->setAttribute($attributeCode);

        $values = $this->getAttributeValueLabel($attributeCode);
        $values = is_array($values) ? $values : [$values];
        $values = array_filter(array_map('trim', $values));
        $attributeValue->setValues($values);
        return $attributeValue;
    }

    /**
     * @return Collection
     */
    public function getCollection()
    {
        return $this->collection->getCollection();
    }

    /**
     * @return Configurable
     */
    public function getTypeConfigurable()
    {
        return $this->typeConfigurable;
    }

    /**
     * @param Configurable $typeConfigurable
     */
    public function setTypeConfigurable($typeConfigurable): self
    {
        $this->typeConfigurable = $typeConfigurable;
        return $this;
    }

    /**
     * @return ProductRepositoryInterface
     */
    public function getProductRepository()
    {
        return $this->productRepository;
    }

    /**
     * @param ProductRepositoryInterface $productRepository
     */
    public function setProductRepository($productRepository): self
    {
        $this->productRepository = $productRepository;
        return $this;
    }
}
