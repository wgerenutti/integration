<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product;

use function get_class;
use function is_array;
use function is_string;
use function sprintf;

class GenericHydrator extends AbstractHydrator
{
    /** @var string */
    protected $field;

    public function hydrate(object $object)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($object),
                static::class
            )
        );
        $value = $this->getAttributeValueLabel(
            $this->getField()
        ) ?: $this->getRawAttributeValue(
            $this->getField()
        );

        if (is_array($value)) {
            $value = '';
        }

        if (! is_string($value)) {
            $value = (string) $value;
        }

        $object->setData(
            $this->getField(),
            $value
        );
    }

    /**
     * @return mixed
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param mixed $field
     */
    public function setField($field): self
    {
        $this->field = $field;
        return $this;
    }
}
