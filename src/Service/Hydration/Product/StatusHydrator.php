<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product;

use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Config\Attribute;
use Gubee\SDK\DataObject\Product;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Psr\Log\LoggerInterface;

use function get_class;
use function sprintf;

class StatusHydrator extends AbstractHydrator
{
    /** @var StockRegistryInterface */
    protected $stockRegistry;

    public function __construct(
        StockRegistryInterface $stockRegistry,
        ProductInterface $product,
        Config $config,
        Attribute $attributeConfig,
        LoggerInterface $logger
    ) {
        $this->stockRegistry = $stockRegistry;
        parent::__construct(
            $product,
            $config,
            $attributeConfig,
            $logger
        );
    }

    public function hydrate(object $object)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($object),
                static::class
            )
        );
        if (! $this->getProduct()->isSalable()) {
            $this->getLogger()->debug(
                sprintf(
                    "Product '%s' is not salable",
                    $this->getProduct()->getSku()
                )
            );
            return $object->setStatus(Product::INACTIVE);
        }

        if (! $this->getStockItem()->getIsInStock()) {
            $this->getLogger()->debug(
                sprintf(
                    "Product '%s' is not in stock",
                    $this->getProduct()->getSku()
                )
            );
            return $object->setStatus(Product::INACTIVE);
        }

        $this->getLogger()->debug(
            sprintf(
                "Product '%s' is active",
                $this->getProduct()->getSku()
            )
        );
        return $object->setStatus(Product::ACTIVE);
    }

    /**
     * @return StockItemInterface
     */
    public function getStockItem()
    {
        return $this->getStockRegistry()->getStockItem(
            $this->getProduct()->getId()
        );
    }

    /**
     * @return StockRegistryInterface
     */
    public function getStockRegistry()
    {
        return $this->stockRegistry;
    }

    /**
     * @param StockRegistryInterface $stockRegistry
     */
    public function setStockRegistry($stockRegistry): self
    {
        $this->stockRegistry = $stockRegistry;
        return $this;
    }
}
