<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product;

use function get_class;
use function sprintf;

class OriginHydrator extends AbstractHydrator
{
    public function hydrate(object $object)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($object),
                static::class
            )
        );
        $value = $this->getRawAttributeValue(
            'gubee_origin'
        );

        if ($value) {
            return $object->setOrigin(
                (string) $value
            );
        }
    }
}
