<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product;

use Gubee\SDK\DataObject\Product;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable as ConfigurableProductType;

use function count;
use function get_class;
use function sprintf;

class TypeHydrator extends AbstractHydrator
{
    public function hydrate(object $object)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($object),
                static::class
            )
        );

        if ($this->getProduct()->getTypeId() == ConfigurableProductType::TYPE_CODE) {
            $this->getLogger()->debug(
                sprintf(
                    "Product '%s' is a configurable product",
                    $this->getProduct()->getSku()
                )
            );
            return $object->setType(Product::VARIANT);
        }
        $parents = $this->getProduct()
            ->getTypeInstance()
            ->getParentIdsByChild(
                $this->getProduct()->getId()
            );
        if (count($parents) > 0) {
            $this->getLogger()->debug(
                sprintf(
                    "Product '%s' is a variant product base on number of parents",
                    $this->getProduct()->getSku()
                )
            );
            return $object->setType(Product::VARIANT);
        }
        $this->getLogger()->debug(
            sprintf(
                "Product '%s' is a simple product",
                $this->getProduct()->getSku()
            )
        );
        return $object->setType(Product::SIMPLE);
    }
}
