<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product;

use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Config\Attribute;
use Gubee\Integration\Service\Hydration\HydratorInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Psr\Log\LoggerInterface;

use function array_filter;
use function array_map;
use function explode;
use function is_array;

abstract class AbstractHydrator implements HydratorInterface
{
    /** @var ProductInterface */
    protected $product;

    /** @var Config */
    protected $config;

    /** @var Attribute */
    protected $attributeConfig;

    /** @var LoggerInterface */
    protected $logger;

    public function __construct(
        ProductInterface $product,
        Config $config,
        Attribute $attributeConfig,
        LoggerInterface $logger
    ) {
        $this->product         = $product;
        $this->config          = $config;
        $this->attributeConfig = $attributeConfig;
        $this->logger          = $logger;
    }

    public function getRawAttributeValue($attributeCode)
    {
        $attribute = $this->getProduct()->getResource()
            ->getAttribute($attributeCode);
        if (! $attribute) {
            return null;
        }
        $frontend = $attribute->getFrontend();
        if (! $frontend) {
            return null;
        }

        if ($attribute->getFrontendInput() === 'multiselect') {
            $value = $this->getProduct()->getData($attributeCode);
            if (! $value) {
                return null;
            }
            if (is_array($value)) {
                return $value;
            }
            $value = explode(',', $value ?: '');
            $value = array_filter($value);
            if (! $value) {
                return null;
            }
            $value = array_map(function ($value) use ($frontend) {
                return $frontend->getOption($value);
            }, $value ?: []);

            return $value;
        }

        return $this->getProduct()->getResource()
            ->getAttributeRawValue(
                $this->getProduct()->getId(),
                $attributeCode,
                $this->getProduct()->getStoreId()
            );
    }

    public function getAttributeValueLabel($attributeCode)
    {
        $attribute = $this->getProduct()->getResource()
            ->getAttribute($attributeCode);
        if (! $attribute) {
            return null;
        }
        $frontend = $attribute->getFrontend();
        if (! $frontend) {
            return null;
        }

        // check if attribute is multselect
        if ($attribute->getFrontendInput() === 'multiselect') {
            $value = $this->getRawAttributeValue($attributeCode);
            if (! $value) {
                return null;
            }
            if (is_array($value)) {
                return $value;
            }
            $value = explode(',', $value ?: '');
            $value = array_filter($value);
            if (! $value) {
                return null;
            }
            $value = array_map(function ($value) use ($frontend) {
                return $frontend->getOption($value);
            }, $value ?: []);

            return $value;
        }

        return $frontend->getValue(
            $this->getProduct()
        );
    }

    /**
     * @return ProductInterface
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param ProductInterface $product
     */
    public function setProduct($product): self
    {
        $this->product = $product;
        return $this;
    }

    /**
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Config $config
     */
    public function setConfig($config): self
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @return Attribute
     */
    public function getAttributeConfig()
    {
        return $this->attributeConfig;
    }

    /**
     * @param Attribute $attributeConfig
     */
    public function setAttributeConfig($attributeConfig): self
    {
        $this->attributeConfig = $attributeConfig;
        return $this;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger($logger): self
    {
        $this->logger = $logger;
        return $this;
    }
}
