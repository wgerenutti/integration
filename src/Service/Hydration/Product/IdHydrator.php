<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product;

use function get_class;
use function sprintf;

class IdHydrator extends AbstractHydrator
{
    protected $parent;

    public function hydrate(object $object)
    {
        $this->getLogger()->debug(
            sprintf(
                "Hydrating '%s' with '%s'",
                get_class($object),
                static::class
            )
        );

        $id = $this->getProduct()->getData('sku');
        if ($id) {
            if ($this->getParent()) {
                $idParent = $this->getParent()->getData('sku');

                return sprintf(
                    "%s-%s",
                    $idParent,
                    $id
                );
            }

            return $object->setid(
                $id
            );
        }
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent): self
    {
        $this->parent = $parent;
        return $this;
    }
}
