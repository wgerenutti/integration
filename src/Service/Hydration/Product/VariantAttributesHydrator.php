<?php

declare(strict_types=1);

namespace Gubee\Integration\Service\Hydration\Product;

class VariantAttributesHydrator extends SpecificationsHydrator
{
    public function hydrate(object $object)
    {
        $attributes       = $this->getVariantAttributes() ?: [];
        $variantAttribute = [];
        foreach ($attributes as $attribute) {
            $variantAttribute[] = $attribute->getAttribute();
        }

        $object->setVariantAttributes($variantAttribute);
    }
}
