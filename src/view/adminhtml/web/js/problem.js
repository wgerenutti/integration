let GubeeProblems = (() => {
  function init() {
    let validationBtn = document.getElementById("gubee_validation");
    if (!validationBtn) {
      return;
    }

    // get number of errors
    let errors = validationBtn.querySelector("span").innerHTML;
    errors = JSON.parse(errors);
    validationBtn.querySelector("span").innerHTML = "";
    if (errors.errors == 0) {
      return;
    }
    // create badge with number of errors
    let badge = document.createElement("span");
    badge.classList.add("notifications-counter");
    badge.innerHTML = errors.errors;
    validationBtn.appendChild(badge);
    validationBtn.classList.add("gubee-tooltip");
    var div = document.createElement("div");
    div.classList.add("gubee-tooltiptext");
    div.innerHTML = errors.message;
    validationBtn.appendChild(div);
  }

  return {
    init: init,
  };
})();

document.addEventListener("DOMContentLoaded", () => {
  GubeeProblems.init();
});
