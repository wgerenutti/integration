<?php

declare(strict_types=1);

namespace Gubee\Integration\Controller\Adminhtml\Job;

use Exception;
use Gubee\Integration\Api\JobRepositoryInterface;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

use function __;

class Execute extends Action
{
    protected $queueRepository;
    protected Consumer $consumer;

    /**
     * Constructor
     *
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        JobRepositoryInterface $queueRepository,
        Context $context
    ) {
        $this->queueRepository = $queueRepository;
        parent::__construct($context);
    }

    /**
     * Index action
     */
    public function execute()
    {
        $params = $this->getRequest()->getParams();
        if (! isset($params['id'])) {
            $this->messageManager->addErrorMessage(__('Queue item not found'));
            return $this->_redirect('gubee/job/index');
        }
        $queue = $this->getQueueRepository()->get($params['id']);
        if (! $queue->getId()) {
            $this->messageManager->addErrorMessage(__('Queue item not found'));
            return $this->_redirect('gubee/job/index');
        }
        try {
            $queue->process(true);
            $this->messageManager->addSuccessMessage(__('Queue item executed'));
            return $this->_redirect('gubee/job/index');
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage(__('Queue item not executed'));
            $this->messageManager->addErrorMessage(
                $e->getMessage()
            );
            return $this->_redirect('gubee/job/index');
        }
    }

    public function getQueueRepository(): JobRepositoryInterface
    {
        return $this->queueRepository;
    }

    public function setQueueRepository(JobRepositoryInterface $queueRepository): self
    {
        $this->queueRepository = $queueRepository;
        return $this;
    }
}
