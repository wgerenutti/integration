<?php

declare(strict_types=1);

namespace Gubee\Integration\Controller\Adminhtml\Catalog\Category;

use Exception;
use Gubee\Integration\Model\JobFactory;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NotFoundException;

use function __;

class Sync extends Action
{
    protected $queueFactory;

    public function __construct(
        JobFactory $queueFactory,
        Context $context
    ) {
        $this->queueFactory = $queueFactory;
        parent::__construct($context);
    }

    /**
     * Update product(s) status action
     *
     * @throws NotFoundException
     * @return Redirect
     */
    public function execute()
    {
        try {
            $queueItem = $this->getQueueFactory()->create();
            $queueItem->setCode('category:sync');
            $queueItem->setPriority(99);
            $queueItem->save();
            $this->messageManager->addSuccessMessage(__('Category sync queued.'));
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (Exception $e) {
            $this->_getSession()->addException($e, __('Something went wrong while queueing the category sync.'));
        }

        /**
         * @var Redirect $resultRedirect
         */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $resultRedirect->setPath('catalog/category/index');
    }

    /**
     * @return mixed
     */
    public function getQueueFactory()
    {
        return $this->queueFactory;
    }

    /**
     * @param  $queueFactory
     */
    public function setQueueFactory($queueFactory): self
    {
        $this->queueFactory = $queueFactory;
        return $this;
    }
}
