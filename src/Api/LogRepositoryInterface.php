<?php

declare(strict_types=1);

namespace Gubee\Integration\Api;

use Gubee\Integration\Api\Data\LogInterface;
use Gubee\Integration\Api\Data\LogSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

interface LogRepositoryInterface
{
    /**
     * Save Log
     *
     * @return LogInterface
     * @throws LocalizedException
     */
    public function save(
        LogInterface $log
    );

    /**
     * Retrieve Log
     *
     * @param string $logId
     * @return LogInterface
     * @throws LocalizedException
     */
    public function get($logId);

    /**
     * Retrieve Log matching the specified criteria.
     *
     * @return LogSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Log
     *
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(
        LogInterface $log
    );

    /**
     * Delete Log by ID
     *
     * @param string $logId
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($logId);
}
