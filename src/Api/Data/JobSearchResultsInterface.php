<?php

declare(strict_types=1);

namespace Gubee\Integration\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface JobSearchResultsInterface extends SearchResultsInterface
{
    /**
     * Get Job list.
     *
     * @return JobInterface[]
     */
    public function getItems(): array;

    /**
     * Set queue list.
     *
     * @param JobInterface[] $items
     * @return $this
     */
    public function setItems(array $items): array;
}
