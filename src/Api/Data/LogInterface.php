<?php

declare(strict_types=1);

namespace Gubee\Integration\Api\Data;

interface LogInterface
{
    public const LOG_ID     = 'log_id';
    public const MESSAGE    = 'message';
    public const LEVEL      = 'level';
    public const CONTEXT    = 'context';
    public const CREATED_AT = 'created_at';

    public const LEVEL_EMERGENCY = 'emergency';
    public const LEVEL_ALERT     = 'alert';
    public const LEVEL_CRITICAL  = 'critical';
    public const LEVEL_ERROR     = 'error';
    public const LEVEL_WARNING   = 'warning';
    public const LEVEL_NOTICE    = 'notice';
    public const LEVEL_INFO      = 'info';
    public const LEVEL_DEBUG     = 'debug';

    /**
     * Get level
     *
     * @return string|null
     */
    public function getLevel();

    /**
     * Set level
     *
     * @param string $level
     * @return \Gubee\Integration\Log\Api\Data\LogInterface
     */
    public function setLevel($level);

    /**
     * Get log_id
     *
     * @return string|null
     */
    public function getLogId();

    /**
     * Set log_id
     *
     * @param string $logId
     * @return \Gubee\Integration\Log\Api\Data\LogInterface
     */
    public function setLogId($logId);

    /**
     * Get message
     *
     * @return string|null
     */
    public function getMessage();

    /**
     * Set message
     *
     * @param string $message
     * @return \Gubee\Integration\Log\Api\Data\LogInterface
     */
    public function setMessage($message);

    /**
     * Get context
     *
     * @return string|null
     */
    public function getContext();

    /**
     * Set context
     *
     * @param string $context
     * @return \Gubee\Integration\Log\Api\Data\LogInterface
     */
    public function setContext($context);

    /**
     * Get created_at
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Set created_at
     *
     * @param string $createdAt
     * @return \Gubee\Integration\Log\Api\Data\LogInterface
     */
    public function setCreatedAt($createdAt);
}
