<?php

declare(strict_types=1);

namespace Gubee\Integration\Api\Data;

use DateTimeInterface;
use Magento\Framework\DataObject;

interface JobInterface
{
    public const JOB_ID        = 'job_id';
    public const CODE          = 'code';
    public const PAYLOAD       = 'payload';
    public const ATTEMPTS      = 'attempts';
    public const STATUS        = 'status';
    public const PRIORITY      = 'priority';
    public const ERROR_MESSAGE = 'error_message';
    public const CREATED_AT    = 'created_at';
    public const UPDATED_AT    = 'updated_at';

    public const STATUS_PENDING  = 0;
    public const STATUS_RUNNING  = 1;
    public const STATUS_EXECUTED = 2;
    public const STATUS_FAILED   = 3;
    public const STATUS_DONE     = 5;

    public const STATUS_LIST = [
        self::STATUS_PENDING  => 'Pending',
        self::STATUS_RUNNING  => 'Running',
        self::STATUS_EXECUTED => 'Executed',
        self::STATUS_FAILED   => 'Failed',
        self::STATUS_DONE     => 'Done',
    ];

    public const STATUS_PENDING_LIST = [
        self::STATUS_PENDING,
        self::STATUS_RUNNING,
    ];

    public const STATUS_DONE_LIST = [
        self::STATUS_EXECUTED,
        self::STATUS_DONE,
    ];

    public function setJobId(int $jobId): self;

    public function getJobId(): int;

    public function setCode(string $code): self;

    public function getCode(): string;

    public function setPayload(string $payload): self;

    public function getPayload(): DataObject;

    public function setAttempts(int $attempts): self;

    public function getAttempts(): int;

    public function setStatus(int $status): self;

    public function getStatus(): int;

    public function setPriority(int $priority): self;

    public function getPriority(): int;

    public function setErrorMessage(string $errorMessage): self;

    public function getErrorMessage(): string;

    public function setCreatedAt(DateTimeInterface $createdAt): self;

    public function getCreatedAt(): DateTimeInterface;

    public function setUpdatedAt(DateTimeInterface $updatedAt): self;

    public function getUpdatedAt(): DateTimeInterface;
}
