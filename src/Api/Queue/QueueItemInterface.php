<?php

declare(strict_types=1);

namespace Gubee\Integration\Api\Queue;

use Gubee\Integration\Api\Data\JobInterface;

interface QueueItemInterface
{
    public function setJob(JobInterface $job): void;

    public function getJob(): JobInterface;
}
