<?php

declare(strict_types=1);

namespace Gubee\Integration\Api;

use Gubee\Integration\Api\Data\JobInterface;
use Gubee\Integration\Api\Data\JobSearchResultsInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

interface JobRepositoryInterface
{
    /**
     * Save Job
     *
     * @return JobInterface
     * @throws LocalizedException
     */
    public function save(
        JobInterface $job
    );

    /**
     * Retrieve Job
     *
     * @param string $jobId
     * @return JobInterface
     * @throws LocalizedException
     */
    public function get($jobId);

    /**
     * Retrieve Job matching the specified criteria.
     *
     * @return JobSearchResultsInterface
     * @throws LocalizedException
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Job
     *
     * @return bool true on success
     * @throws LocalizedException
     */
    public function delete(
        JobInterface $job
    );

    /**
     * Delete Job by ID
     *
     * @param string $jobId
     * @return bool true on success
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function deleteById($jobId);
}
