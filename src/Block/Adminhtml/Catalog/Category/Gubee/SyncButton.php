<?php

declare(strict_types=1);

namespace Gubee\Integration\Block\Adminhtml\Catalog\Category\Gubee;

use Magento\Backend\Block\Widget\Container;

use function __;
use function sprintf;

class SyncButton extends Container
{
    /**
     * Block constructor adds buttons
     */
    protected function _construct()
    {
        $this->addButton(
            'gubee_sync',
            $this->getButtonData()
        );
        parent::_construct();
    }

    /**
     * Return button attributes array
     */
    public function getButtonData()
    {
        return [
            'label'      => __('Sync to Gubee'),
            'sort_order' => 800,
            'class'      => 'scalable save primary gubee-sync',
            'on_click'   => sprintf("location.href = '%s';", $this->getSyncUrl()),
        ];
    }

    /**
     * @return mixed
     */
    public function getSyncUrl()
    {
        return $this->getUrl('gubee/catalog_category/sync');
    }
}
