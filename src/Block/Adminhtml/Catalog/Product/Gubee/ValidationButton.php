<?php

declare(strict_types=1);

namespace Gubee\Integration\Block\Adminhtml\Catalog\Product\Gubee;

use Gubee\Integration\Service\Model\Product;
use Magento\Backend\Block\Widget\Container;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Registry;
use Magento\Store\Model\App\Emulation;
use Throwable;

use function implode;
use function json_encode;
use function sizeof;
use function sprintf;

class ValidationButton extends Container
{
    /** @var \Magento\Catalog\Model\Product */
    protected $_product;
    /**
     * Core registry
     *
     * @var Registry
     */
    protected $_coreRegistry;
    /**
     * App Emulator
     *
     * @var Emulation
     */
    protected $_emulation;
    /**
     * @param array                                 $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        \Magento\Catalog\Model\Product $product,
        Emulation $emulation,
        array $data = []
    ) {
        $this->_coreRegistry = $registry;
        $this->_product      = $product;
        $this->_request      = $context->getRequest();
        $this->_emulation    = $emulation;
        parent::__construct($context, $data);
    }

    /**
     * Block constructor adds buttons
     */
    protected function _construct()
    {
        $this->addButton(
            'gubee_validation',
            $this->getButtonData()
        );
        parent::_construct();
    }

    /**
     * Only output if product has gubee flagged
     */
    protected function _toHtml()
    {
        $product = $this->_coreRegistry->registry('current_product');
        return parent::_toHtml();
    }

    /**
     * Return button attributes array
     */
    public function getButtonData()
    {
        $problems = $this->validate() ?: [];

        return [
            'label'      => json_encode([
                'errors'  => sizeof($problems),
                'message' => implode("\n", $problems),
            ]),
            'sort_order' => 800,
        ];
    }

    /**
     * Return product frontend url depends on active store
     *
     * @return mixed
     */
    protected function _getProductUrl()
    {
        return null;
    }

    /**
     * @return mixed
     */
    public function validate()
    {
        $problems = [];
        try {
            $product = ObjectManager::getInstance()->create(
                Product::class,
                [
                    'product' => $this->_coreRegistry->registry('current_product'),
                ]
            );
            $errors  = $product->validateProduct();
            foreach ($errors as $error) {
                $problems[] = sprintf(
                    "%s.%s: %s",
                    $error['objectName'],
                    $error['field'],
                    $error['message']
                );
            }
        } catch (Throwable $e) {
        }

        return $problems ?: [];
    }
}
