<?php

declare(strict_types=1);

namespace Gubee\Integration\Ui\Component\Log\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;

use function sprintf;

class Message extends Column
{
    /**
     * Prepare Data Source
     *
     * @param  array   $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $item[$this->getData('name')] = sprintf(
                    "<textarea class='bg-gray-100 border border-gray-300 rounded-lg p-2 w-full'
                    readonly
                     style='min-width:800px;'>%s</textarea>",
                    $item['message']
                );
            }
        }

        return $dataSource;
    }
}
