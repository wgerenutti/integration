<?php

declare(strict_types=1);

namespace Gubee\Integration\Ui\Component\Log\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;

use function json_decode;
use function sprintf;

class Context extends Column
{
    /**
     * Prepare Data Source
     *
     * @param  array   $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $context                      = $item['context'];
                $context                      = json_decode($context, true);
                $item[$this->getData('name')] = $this->collapse($context);
            }
        }

        return $dataSource;
    }

    /**
     * Make context collapse, with table inside
     */
    public function collapse($array)
    {
        /**
         * Add taillwwind down arrow
         */
        return <<<HTML

            <details
            class="border border-gray-200 rounded-md p-4 m-2"
            style='min-width:800px;'>
                <summary>Context <span class="icon-before">▼</span></summary>
                {$this->arrayToTable($array)}
            </details>
        HTML;
    }

    private function arrayToTable($array)
    {
        $html = '';
        foreach ($array as $key => $value) {
            $html .= '<tr>';
            $html .= '<td>' . $key . '</td>';
            $html .= '<td>' . $value . '</td>';
            $html .= '</tr>';
        }
        return sprintf("<table style='min-width:800px;'>%s</table>", $html);
    }
}
