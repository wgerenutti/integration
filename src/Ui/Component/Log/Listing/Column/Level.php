<?php

declare(strict_types=1);

namespace Gubee\Integration\Ui\Component\Log\Listing\Column;

use Gubee\Integration\Api\Data\LogInterface;
use Magento\Ui\Component\Listing\Columns\Column;

use function __;
use function sprintf;

class Level extends Column
{
    /**
     * Prepare Data Source
     *
     * @param  array   $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $item[$this->getData('name')] = $this->getLabel($item['level']);
            }
        }

        return $dataSource;
    }

    /**
     * @param $status
     */
    public function getLabel($status)
    {
        switch ($status) {
            case LogInterface::LEVEL_DEBUG:
                return sprintf(
                    "<span class='grid-severity-minor'><span>%s</span></span>",
                    __('Debug')
                );
            case LogInterface::LEVEL_WARNING:
                return sprintf(
                    "<span class='grid-severity-notice'><span>%s</span></span>",
                    __('Warning')
                );
            case LogInterface::LEVEL_NOTICE:
                return sprintf(
                    "<span class='grid-severity-notice'><span>%s</span></span>",
                    __('Notice')
                );
            case LogInterface::LEVEL_INFO:
                return sprintf(
                    "<span class='grid-severity-notice'><span>%s</span></span>",
                    __('Info')
                );
            case LogInterface::LEVEL_EMERGENCY:
                return sprintf(
                    "<span class='grid-severity-critical'><span>%s</span></span>",
                    __('Error')
                );
            case LogInterface::LEVEL_ALERT:
                return sprintf(
                    "<span class='grid-severity-notice'><span>%s</span></span>",
                    __('Info')
                );
            case LogInterface::LEVEL_CRITICAL:
                return sprintf(
                    "<span class='grid-severity-critical'><span>%s</span></span>",
                    __('Error')
                );
            case LogInterface::LEVEL_ERROR:
                return sprintf(
                    "<span class='grid-severity-critical'><span>%s</span></span>",
                    __('Error')
                );
            default:
                return sprintf(
                    "<span class='grid-severity-minor'><span>%s</span></span>",
                    __('Debug')
                );
        }
    }
}
