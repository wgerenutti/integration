<?php

declare(strict_types=1);

namespace Gubee\Integration\Ui\Component\Job\Listing\Column;

use Gubee\Integration\Api\Data\JobInterface;
use Magento\Ui\Component\Listing\Columns\Column;

use function __;
use function sprintf;

class Status extends Column
{
    /**
     * Prepare Data Source
     *
     * @param  array   $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                // here we can also use the data from $item to configure some parameters of an action URL
                $item[$this->getData('name')] = $this->getLabel($item['status']);
            }
        }

        return $dataSource;
    }

    /**
     * @param $status
     */
    public function getLabel($status)
    {
        switch ($status) {
            case JobInterface::STATUS_PENDING:
                return sprintf(
                    "<span class='grid-severity-minor'><span>%s</span></span>",
                    __('Pending')
                );
            case JobInterface::STATUS_RUNNING:
                return sprintf(
                    "<span class='grid-severity-notice'><span>%s</span></span>",
                    __('Processing')
                );
            case JobInterface::STATUS_DONE:
                return sprintf(
                    "<span class='grid-severity-notice'><span>%s</span></span>",
                    __('Complete')
                );
            case JobInterface::STATUS_FAILED:
                return sprintf(
                    "<span class='grid-severity-critical'><span>%s</span></span>",
                    __('Error')
                );
            default:
                return sprintf(
                    "<span class='grid-severity-minor'><span>%s</span></span>",
                    __('Unknown')
                );
        }
    }
}
