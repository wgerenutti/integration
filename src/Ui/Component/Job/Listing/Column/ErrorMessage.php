<?php

declare(strict_types=1);

namespace Gubee\Integration\Ui\Component\Job\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;

use function json_decode;
use function json_last_error;

use const JSON_ERROR_NONE;

class ErrorMessage extends Column
{
    /**
     * Prepare Data Source
     *
     * @param  array   $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                // here we can also use the data from $item to configure some parameters of an action URL
                if (! $this->isJson($item['error_message'])) {
                    continue;
                }
                $array                 = json_decode($item['error_message'] ?: '', true);
                $item['error_message'] = '';
                foreach ($array ?: [] as $name => $content) {
                    if ($name == 'error_message') {
                        $item['error_message'] .= $content;
                        continue;
                    }
                    $item['error_message'] .= $this->collapse(
                        $name,
                        $content
                    );
                }
            }
        }

        return $dataSource;
    }

    private function isJson($content)
    {
        if (!$content) {
            return false;
        }
        json_decode($content);
        return json_last_error() === JSON_ERROR_NONE;
    }

    /**
     * Make context collapse, with table inside
     */
    public function collapse($name, $content)
    {
        /**
         * Add taillwwind down arrow
         */
        return <<<HTML
            <details
            class="border border-gray-200 rounded-md p-4 m-2"
            style='min-width:800px;'>
                <summary>{$name}<span class="icon-before">▼</span></summary>
                {$content}
            </details>
        HTML;
    }
}
