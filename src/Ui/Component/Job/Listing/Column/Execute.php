<?php

declare(strict_types=1);

namespace Gubee\Integration\Ui\Component\Job\Listing\Column;

use Magento\Ui\Component\Listing\Columns\Column;

use function __;

class Execute extends Column
{
    /**
     * Prepare Data Source
     *
     * @param  array   $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as &$item) {
                $item[$this->getData('name')]['execute'] = [
                    'href'  => $this->getContext()->getUrl(
                        'gubee/job/execute',
                        [
                            'id' => $item['job_id'],
                        ]
                    ),
                    'label' => __('Execute'),
                ];
                if ($item['payload'] == '') {
                    continue;
                }
            }
        }

        return $dataSource;
    }
}
