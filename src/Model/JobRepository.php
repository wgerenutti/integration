<?php

declare(strict_types=1);

namespace Gubee\Integration\Model;

use Exception;
use Gubee\Integration\Api\Data\JobInterface;
use Gubee\Integration\Api\Data\JobInterfaceFactory;
use Gubee\Integration\Api\Data\JobSearchResultsInterfaceFactory;
use Gubee\Integration\Api\JobRepositoryInterface;
use Gubee\Integration\Model\ResourceModel\Job as ResourceJob;
use Gubee\Integration\Model\ResourceModel\Job\CollectionFactory as JobCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

use function __;

class JobRepository implements JobRepositoryInterface
{
    /** @var CollectionProcessorInterface */
    protected $collectionProcessor;

    /** @var JobInterfaceFactory */
    protected $jobFactory;

    /** @var Job */
    protected $searchResultsFactory;

    /** @var ResourceJob */
    protected $resource;

    /** @var JobCollectionFactory */
    protected $jobCollectionFactory;

    public function __construct(
        ResourceJob $resource,
        JobInterfaceFactory $jobFactory,
        JobCollectionFactory $jobCollectionFactory,
        JobSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->resource             = $resource;
        $this->jobFactory           = $jobFactory;
        $this->jobCollectionFactory = $jobCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor  = $collectionProcessor;
    }

    /**
     * @inheritDoc
     */
    public function save(JobInterface $job)
    {
        try {
            $this->resource->save($job);
        } catch (Exception $exception) {
            throw new CouldNotSaveException(
                __(
                    'Could not save the job: %1',
                    $exception->getMessage()
                )
            );
        }
        return $job;
    }

    /**
     * @inheritDoc
     */
    public function get($jobId)
    {
        $job = $this->jobFactory->create();
        $this->resource->load($job, $jobId);
        if (! $job->getId()) {
            throw new NoSuchEntityException(
                __(
                    'Job with id "%1" does not exist.',
                    $jobId
                )
            );
        }
        return $job;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        SearchCriteriaInterface $criteria
    ) {
        $collection = $this->jobCollectionFactory->create();

        $this->collectionProcessor->process($criteria, $collection);

        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);

        $items = [];
        foreach ($collection as $model) {
            $items[] = $model;
        }

        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @inheritDoc
     */
    public function delete(JobInterface $job)
    {
        try {
            $jobModel = $this->jobFactory->create();
            $this->resource->load($jobModel, $job->getJobId());
            $this->resource->delete($jobModel);
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(
                __(
                    'Could not delete the Job: %1',
                    $exception->getMessage()
                )
            );
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($jobId)
    {
        return $this->delete($this->get($jobId));
    }
}
