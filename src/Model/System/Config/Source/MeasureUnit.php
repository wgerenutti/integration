<?php

declare(strict_types=1);

namespace Gubee\Integration\Model\System\Config\Source;

use Gubee\SDK\DataObject\Product\Attribute\Dimension\Measure;
use Magento\Framework\Option\ArrayInterface;

use function __;

class MeasureUnit implements ArrayInterface
{
    /**
     * @return mixed
     */
    public function toOptionArray()
    {
        $options = [];
        foreach (
            [
                Measure::CENTIMETER => __('Centimeter'),
                Measure::METER      => __('Meter'),
                Measure::MILLIMETER => __('Millimeter'),
            ] as $key => $value
        ) {
            $options[] = [
                'label' => $value,
                'value' => $key,
            ];
        }

        return $options;
    }
}
