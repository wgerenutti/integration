<?php

declare(strict_types=1);

namespace Gubee\Integration\Model\System\Config\Source\Product;

use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Eav\Model\Entity\Type;
use Magento\Framework\Option\ArrayInterface;

use function array_column;
use function array_combine;

abstract class AbstractAttibuteOptions implements ArrayInterface
{
    /** @var mixed */
    protected $entityType;

    /** @var Attribute */
    private $attributeResource;

    public function __construct(
        Attribute $attributeResource,
        Type $entityType
    ) {
        $this->attributeResource = $attributeResource;
        $this->entityType        = $entityType;
    }

    abstract public function toOptionArray();

    public function toArray()
    {
        return array_combine(
            array_column($this->toOptionArray(), 'value'),
            array_column($this->toOptionArray(), 'label')
        );
    }

    /**
     * Get the value of attributeResource
     *
     * @return Attribute
     */
    public function getAttributeResource()
    {
        return $this->attributeResource;
    }

    /**
     * Get the value of entityType
     */
    public function getEntityType()
    {
        return $this->entityType;
    }

    /**
     * Set the value of attributeResource
     *
     * @return self
     */
    public function setAttributeResource(Attribute $attributeResource)
    {
        $this->attributeResource = $attributeResource;

        return $this;
    }

    /**
     * Set the value of entityType
     *
     * @return self
     */
    public function setEntityType(Type $entityType)
    {
        $this->entityType = $entityType;

        return $this;
    }
}
