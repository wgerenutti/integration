<?php

declare(strict_types=1);

namespace Gubee\Integration\Model\System\Config\Source\Product\Attribute;

use Gubee\Integration\Model\System\Config\Source\Product\Attribute;

class Price extends Attribute
{
    protected function getCollection()
    {
        return parent::getCollection()->addFieldToFilter(
            'frontend_input',
            'price'
        );
    }
}
