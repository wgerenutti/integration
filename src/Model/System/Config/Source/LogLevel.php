<?php

declare(strict_types=1);

namespace Gubee\Integration\Model\System\Config\Source;

use Magento\Framework\Option\ArrayInterface;
use ReflectionClass;

use function __;
use function array_column;
use function array_combine;
use function ucfirst;

class LogLevel implements ArrayInterface
{
    public function toOptionArray(): array
    {
        $logLevels = new ReflectionClass(\Psr\Log\LogLevel::class);
        $options   = [];
        foreach ($logLevels->getConstants() as $level) {
            $options[] = [
                'value' => $level,
                'label' => __(ucfirst($level)),
            ];
        }

        return $options;
    }

    public function toArray()
    {
        return array_combine(
            array_column($this->toOptionArray(), 'value'),
            array_column($this->toOptionArray(), 'label')
        );
    }
}
