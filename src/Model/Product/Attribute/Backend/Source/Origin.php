<?php

declare(strict_types=1);

namespace Gubee\Integration\Model\Product\Attribute\Backend\Source;

use Gubee\SDK\DataObject\Product;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

use function __;

class Origin extends AbstractSource
{
    /**
     * @return array
     */
    public function getAllOptions()
    {
        $options = [];
        foreach (
            [
                Product::FOREIGN_DIRECTION_IMPORTATION => 'Foreing direction importation',
                Product::FOREIGN_INTERNAL_MARKET       => 'Foreing internal market',
                Product::NATIONAL                      => 'National',
            ] as $key => $origin
        ) {
            $options[] = [
                'label' => __($origin),
                'value' => $key,
            ];
        }

        return $options;
    }
}
