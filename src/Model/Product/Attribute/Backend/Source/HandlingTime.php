<?php

declare(strict_types=1);

namespace Gubee\Integration\Model\Product\Attribute\Backend\Source;

use Gubee\SDK\DataObject\Product\Attribute\Dimension\UnitTime;
use Magento\Eav\Model\Entity\Attribute\Source\AbstractSource;

use function __;

class HandlingTime extends AbstractSource
{
    /**
     * @return array
     */
    public function getAllOptions()
    {
        $options = [];
        foreach (
            [
                UnitTime::DAYS  => __('Days'),
                UnitTime::HOURS => __('Hours'),
                UnitTime::MONTH => __('Month'),
            ] as $key => $origin
        ) {
            $options[] = [
                'label' => $origin,
                'value' => $key,
            ];
        }

        return $options;
    }
}
