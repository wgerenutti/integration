<?php

declare(strict_types=1);

namespace Gubee\Integration\Model\ResourceModel\Job;

use Gubee\Integration\Api\Data\JobInterface;
use Gubee\Integration\Model\Job;
use Gubee\Integration\Model\ResourceModel\Job as JobResource;
use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /** @inheritDoc */
    protected $_idFieldName = JobInterface::JOB_ID;

    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init(
            Job::class,
            JobResource::class
        );
    }
}
