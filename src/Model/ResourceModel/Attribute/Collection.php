<?php

declare(strict_types=1);

namespace Gubee\Integration\Model\ResourceModel\Attribute;

use Gubee\Integration\Helper\Config\Attribute;
use Magento\Catalog\Model\ResourceModel\Product\Attribute\Collection as ProductAttributeCollection;

class Collection
{
    /** @var ProductAttributeCollection */
    protected $collection;

    protected $attributeConfig;

    public function __construct(
        ProductAttributeCollection $collection,
        Attribute $attributeConfig
    ) {
        $this->attributeConfig = $attributeConfig;
        $this->collection      = $collection;
    }

    public function getCollection()
    {
        return $this->collection
        ->addFieldToFilter(
            'attribute_code',
            [
                'nlike' => 'gubee_%',
                'neq'   => $this->attributeConfig->getBrand(),
            ]
        )->addFieldToFilter(
            'frontend_label',
            ['notnull' => true]
        )->addFieldToFilter(
            'frontend_input',
            [
                'neq' => 'gallery',
            ]
        )->addFieldToFilter(
            'backend_type',
            ['neq' => 'static']
        );
    }
}
