<?php

declare(strict_types=1);

namespace Gubee\Integration\Model\ResourceModel;

use Gubee\Integration\Api\Data\JobInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Job extends AbstractDb
{
    /**
     * @inheritDoc
     */
    protected function _construct()
    {
        $this->_init('gubee_integration_queue_job', JobInterface::JOB_ID);
    }
}
