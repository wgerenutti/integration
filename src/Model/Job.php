<?php

declare(strict_types=1);

namespace Gubee\Integration\Model;

use DateTimeInterface;
use Gubee\Integration\Api\Data\JobInterface;
use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Log;
use Gubee\Integration\Model\ResourceModel\Job as JobResource;
use InvalidArgumentException;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\Collection\AbstractDb;
use Magento\Framework\DataObject;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Model\ResourceModel\AbstractResource;
use Magento\Framework\Registry;

use function __;
use function array_keys;
use function implode;
use function in_array;
use function json_decode;
use function json_last_error;
use function json_last_error_msg;
use function sprintf;

use const JSON_ERROR_NONE;

class Job extends AbstractModel implements JobInterface
{
    /** @var Config */
    protected $config;

    /** @var Log */
    protected $logger;

    protected $processors = [];

    /**
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        Config $config,
        Log $log,
        ?AbstractResource $resource = null,
        ?AbstractDb $resourceCollection = null,
        array $data = [],
        array $processors = []
    ) {
        $this->config     = $config;
        $this->log        = $log;
        $this->processors = $processors;
        parent::__construct(
            $context,
            $registry,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * @inheritDoc
     */
    public function _construct()
    {
        $this->_init(JobResource::class);
    }

    public function beforeSave()
    {
        /**
         * Check if same job is already on queue
         */
        $collection = $this->getCollection()
            ->addFieldToFilter(
                'code',
                $this->getCode()
            )->addFieldToFilter(
                'payload',
                $this->getData(self::PAYLOAD)
            )->addFieldToFilter(
                'status',
                [
                    'nin' => self::STATUS_DONE_LIST,
                ]
            )->load();
        if ($collection->getSize() > 0) {
            $this->setId(
                $collection->getFirstItem()->getId()
            );
        }
    }

    public function process($force = false)
    {
        $job = ObjectManager::getInstance()->create(
            $this->getCodeProcessor(),
            [
                'jobItem' => $this,
            ]
        );
        return $job->execute($force);
    }

    public function getCodeProcessor()
    {
        return $this->processors[$this->getCode()];
    }

    public function setStatus(int $status): self
    {
        if (
            ! in_array(
                $status,
                array_keys(self::STATUS_LIST)
            )
        ) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid status "%s". Allowed values are: %s',
                    $status,
                    '"' . implode('", "', array_keys(self::STATUS_LIST)) . '"'
                )
            );
        }
        return $this->setData(self::STATUS, $status);
    }

    public function getStatusLabel(int $status): string
    {
        return __(self::STATUS_LIST[$status]);
    }

    public function getStatus(): int
    {
        return $this->getData(self::STATUS);
    }

    public function fail($e = null)
    {
        $this->setStatus(self::STATUS_FAILED);
        $this->setErrorMessage($e->getMessage());
        $this->save();
    }

    public function setJobId(int $jobId): self
    {
        return $this->setData(self::JOB_ID, $jobId);
    }

    public function getJobId(): int
    {
        return $this->getData(self::JOB_ID);
    }

    public function setCode(string $code): self
    {
        if (! $this->processors[$code]) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid code "%s". Allowed values are: %s',
                    $code,
                    implode(', ', array_keys($this->processors))
                )
            );
        }
        return $this->setData(self::CODE, $code);
    }

    public function getCode(): string
    {
        return $this->getData(self::CODE);
    }

    public function setPayload(string $payload): self
    {
        json_decode($payload);
        if (json_last_error() !== JSON_ERROR_NONE) {
            throw new InvalidArgumentException(
                sprintf(
                    'Invalid payload. Error: %s',
                    json_last_error_msg()
                )
            );
        }

        return $this->setData(self::PAYLOAD, $payload);
    }

    public function getPayload(): DataObject
    {
        $array = json_decode($this->getData(self::PAYLOAD), true);
        return new DataObject($array);
    }

    public function setAttempts(int $attempts): self
    {
        return $this->setData(self::ATTEMPTS, $attempts);
    }

    public function getAttempts(): int
    {
        return (int) $this->getData(self::ATTEMPTS);
    }

    public function setPriority(int $priority): self
    {
        return $this->setData(self::PRIORITY, $priority);
    }

    public function getPriority(): int
    {
        return $this->getData(self::PRIORITY);
    }

    public function setErrorMessage(string $errorMessage): self
    {
        return $this->setData(self::ERROR_MESSAGE, $errorMessage);
    }

    public function getErrorMessage(): string
    {
        return $this->getData(self::ERROR_MESSAGE);
    }

    public function setCreatedAt(DateTimeInterface $createdAt): self
    {
        return $this->setData(self::CREATED_AT, $createdAt);
    }

    public function getCreatedAt(): DateTimeInterface
    {
        return $this->getData(self::CREATED_AT);
    }

    public function setUpdatedAt(DateTimeInterface $updatedAt): self
    {
        return $this->setData(self::UPDATED_AT, $updatedAt);
    }

    public function getUpdatedAt(): DateTimeInterface
    {
        return $this->getData(self::UPDATED_AT);
    }

    public function getMaxAttempts()
    {
        return $this->config->getMaxAttempts() ?: 3;
    }

    /**
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Config $config
     */
    public function setConfig($config): self
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @return Log
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param Log $logger
     */
    public function setLogger($logger): self
    {
        $this->logger = $logger;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProcessors()
    {
        return $this->processors;
    }

    /**
     * @param mixed $processors
     */
    public function setProcessors($processors): self
    {
        $this->processors = $processors;
        return $this;
    }
}
