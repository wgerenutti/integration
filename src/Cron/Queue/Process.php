<?php

declare(strict_types=1);

namespace Gubee\Integration\Cron\Queue;

use Gubee\Integration\Console\Command\Queue\ConsumeCommand;
use Magento\Framework\App\ObjectManager;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;

use function ob_end_clean;
use function ob_get_contents;
use function ob_start;

class Process
{
    public function execute()
    {
        $sendCommand = ObjectManager::getInstance()->create(
            ConsumeCommand::class
        );
        $input       = new ArrayInput([]);
        $output      = new ConsoleOutput();
        ob_start();
        $sendCommand->run(
            $input,
            $output
        );
        $content = ob_get_contents();
        ob_end_clean();
        return $content;
    }
}
