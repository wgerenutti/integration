<?php

declare(strict_types=1);

namespace Gubee\Integration\Job;

use Exception;
use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Log;
use Gubee\Integration\Model\Job;
use Psr\Log\LoggerInterface;

use function __;
use function json_encode;
use function method_exists;
use function sprintf;
use function str_repeat;
use function strlen;
use function var_export;

abstract class JobAbstract
{
    /** @var Job */
    protected $jobItem;

    /** @var LoggerInterface */
    protected $logger;

    /** @var Config */
    protected $config;

    public function __construct(
        Job $jobItem,
        Log $logger,
        Config $config
    ) {
        $this->jobItem = $jobItem;
        $this->config  = $config;
        $this->setLogger($logger);
    }

    abstract protected function handle($force = false);

    public function execute($force = false)
    {
        $this->getLogger()->info(
            __(
                '%s(%s)',
                static::class . "::" . __FUNCTION__,
                var_export($force, true)
            )
        );
        if ($this->hasExceededMaxAttempts($force)) {
            $this->getLogger()->info(
                sprintf(
                    "%s - %s",
                    static::class . "::" . __FUNCTION__,
                    "Job has exceeded max attempts"
                )
            );
            $this->getJobItem()->setErrorMessage(
                (string) sprintf('Job has exceeded max attempts')
            );
            $this->getJobItem()->setStatus(Job::STATUS_FAILED);
            $this->getJobItem()->save();
            return;
        }
        $this->getLogger()->info(
            sprintf(
                "%s - %s",
                static::class . "::" . __FUNCTION__,
                "Job is being executed"
            )
        );
        try {
            $this->getJobItem()->setErrorMessage('');
            $this->handle($force);
            $this->getJobItem()->setStatus(Job::STATUS_DONE);
        } catch (Exception $e) {
            $this->getLogger()->error(
                sprintf(
                    "%s:"
                    . str_repeat(" ", 70 - strlen(static::class . "::" . __FUNCTION__)) . "%s",
                    static::class . "::" . __FUNCTION__,
                    $e->getMessage()
                )
            );
            $this->getJobItem()->setErrorMessage(
                json_encode(
                    [
                        'error_message' => $e->getMessage(),
                        'payload'       => method_exists($e, 'getRequest') ? (string) $e->getRequest()->getBody() : null,
                        'response'      => method_exists($e, 'getResponse') ? (string) $e->getResponse()->getBody() : null,
                    ]
                )
            );
            $this->getJobItem()->setStatus(Job::STATUS_FAILED);
        } finally {
            $this->getJobItem()->setAttempts($this->getJobItem()->getAttempts() + 1);
            $this->getJobItem()->save();
        }
    }

    public function hasExceededMaxAttempts($force = false): bool
    {
        return $force
            ? false
            : $this->getJobItem()->getAttempts() >= ($this->getConfig()->getMaxAttempts() ?: 3);
    }

    /**
     * @return Job
     */
    public function getJobItem()
    {
        return $this->jobItem;
    }

    public function setJobItem(Job $jobItem): self
    {
        $this->jobItem = $jobItem;
        return $this;
    }

    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    public function setLogger(LoggerInterface $logger): self
    {
        $this->logger = $logger;
        return $this;
    }

    /**
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Config $config
     */
    public function setConfig($config): self
    {
        $this->config = $config;
        return $this;
    }
}
