<?php

declare(strict_types=1);

namespace Gubee\Integration\Job\Catalog\Category;

use Gubee\Integration\Console\Command\Catalog\Category\SyncCommand;
use Gubee\Integration\Job\JobAbstract;
use Magento\Framework\App\ObjectManager;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;

class Sync extends JobAbstract
{
    protected function handle($force = false)
    {
        $sendCommand = ObjectManager::getInstance()->create(
            SyncCommand::class
        );
        $input       = new ArrayInput([]);
        $output      = new ConsoleOutput();
        return $sendCommand->run(
            $input,
            $output
        );
    }
}
