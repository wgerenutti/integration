<?php

declare(strict_types=1);

namespace Gubee\Integration\Job\Catalog\Product\Price;

use Gubee\Integration\Console\Command\Catalog\Product\Price\SendCommand;
use Gubee\Integration\Job\JobAbstract;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\ObjectManager;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;

class Send extends JobAbstract
{
    protected function handle($force = false)
    {
        $productRepository = ObjectManager::getInstance()->create(ProductRepositoryInterface::class);
        $product           = $productRepository->getById($this->getJobItem()->getPayload()->getProductId());
        $sendCommand       = ObjectManager::getInstance()->create(
            SendCommand::class,
            [
                'product' => $product,
            ]
        );
        $input             = new ArrayInput([]);
        $output            = new ConsoleOutput();
        return $sendCommand->run(
            $input,
            $output
        );
    }
}
