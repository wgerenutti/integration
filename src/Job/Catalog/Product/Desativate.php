<?php

declare(strict_types=1);

namespace Gubee\Integration\Job\Catalog\Product;

use Gubee\Integration\Console\Command\Catalog\Product\DesativateCommand;
use Gubee\Integration\Job\JobAbstract;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\ObjectManager;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\ConsoleOutput;

class Desativate extends JobAbstract
{
    protected function handle($force = false)
    {
        $productRepository = ObjectManager::getInstance()->create(ProductRepositoryInterface::class);
        $productId         = $this->getJobItem()->getPayload()->getProductId();
        $desativateCommand = ObjectManager::getInstance()->create(
            DesativateCommand::class,
            [
                'productId' => $productId,
            ]
        );
        $input             = new ArrayInput([]);
        $output            = new ConsoleOutput();
        return $desativateCommand->run(
            $input,
            $output
        );
    }
}
