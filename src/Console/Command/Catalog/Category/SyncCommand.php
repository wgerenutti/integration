<?php

declare(strict_types=1);

namespace Gubee\Integration\Console\Command\Catalog\Category;

use Exception;
use Gubee\Integration\Console\Command\AbstractCommand;
use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Log;
use Gubee\Integration\Service\Model\Product\Category;
use Gubee\SDK\Api\Product\CategoryApi;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\App\ObjectManager;
use Psr\Log\LoggerInterface;

use function sprintf;

class SyncCommand extends AbstractCommand
{
    protected $categoryCollectionFactory;

    /**
     * @param LoggerInterface   $logger
     * @param $name
     */
    public function __construct(
        CollectionFactory $categoryCollectionFactory,
        Config $config,
        Log $logger,
        $name = null
    ) {
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        parent::__construct($config, $logger, $name);
    }

    protected function configure()
    {
        $this->setName('catalog:category:sync');
        $this->setDescription('Sync category from Gubee API');
        parent::configure();
    }

    protected function _execute(): int
    {
        $collection = $this->getCollection();
        $this->getOutput()->writeln(
            sprintf(
                "Found %s categories to sync",
                $collection->getSize()
            )
        );
        $categories = [];
        foreach ($collection as $category) {
            $categoryGubee = ObjectManager::getInstance()->create(
                Category::class,
                [
                    'category' => $category,
                ]
            );
            $categories[]  = $categoryGubee;
        }

        try {
            if (empty($categories)) {
                $this->getOutput()->writeln("No categories to sync!");
                return self::SUCCESS;
            }
            $categoryApi = ObjectManager::getInstance()->create(
                CategoryApi::class
            );
            $categoryApi->bulkUpdate($categories);
            $this->getOutput()->writeln("All categories synced!");
            return self::SUCCESS;
        } catch (Exception $e) {
            $this->getOutput()->writeln(
                sprintf(
                    "<error>Something went wrong while syncing categories: %s</error>",
                    $e->getMessage()
                )
            );
            throw $e;
        }
    }

    /**
     * @return mixed
     */
    protected function getCollection()
    {
        $collection = $this->categoryCollectionFactory->create();
        $collection->addAttributeToSelect('*')
            ->getSelect()
            ->order('level ASC');
        return $collection;
    }
}
