<?php

declare(strict_types=1);

namespace Gubee\Integration\Console\Command\Catalog\Category;

use Exception;
use Gubee\Integration\Console\Command\AbstractCommand;
use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Log;
use Gubee\Integration\Service\Model\Product\Category;
use Magento\Catalog\Api\Data\CategoryInterface;
use Magento\Framework\App\ObjectManager;

class SendCommand extends AbstractCommand
{
    protected $category;

    public function __construct(
        CategoryInterface $category,
        Config $config,
        Log $logger,
        ?string $name = null
    ) {
        $this->category = ObjectManager::getInstance()->create(
            Category::class,
            [
                'category' => $category,
            ]
        );
        parent::__construct($config, $logger, $name);
    }

    protected function configure()
    {
        $this->setName('catalog:category:send');
        $this->setDescription('Send category to Gubee API');
        parent::configure();
    }

    protected function _execute(): int
    {
        $this->logger->info('Sending category to Gubee API');
        try {
            $this->category->save();
            $this->logger->info('Category sent to Gubee API');
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return self::FAILURE;
        }
        $this->logger->info('Category sent to Gubee API');
        return self::SUCCESS;
    }
}
