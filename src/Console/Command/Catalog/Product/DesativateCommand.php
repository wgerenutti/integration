<?php

declare(strict_types=1);

namespace Gubee\Integration\Console\Command\Catalog\Product;

use Exception;
use Gubee\Integration\Console\Command\AbstractCommand;
use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Log;
use Gubee\Integration\Service\Model\Product;
use Magento\Framework\App\ObjectManager;

class DesativateCommand extends AbstractCommand
{
    protected $productId;
    protected $product;

    public function __construct(
        string $productId,
        Config $config,
        Log $logger,
        ?string $name = null
    ) {
        $this->product   = ObjectManager::getInstance()
            ->create(Product::class);
        $this->productId = $productId;
        parent::__construct($config, $logger, $name);
    }

    protected function configure()
    {
        $this->setName('catalog:product:desativate');
        $this->setDescription('Send product to Gubee API');
        parent::configure();
    }

    protected function _execute(): int
    {
        $this->logger->info('Desativating product on Gubee API');
        try {
            $this->product->desativate($this->productId);
            $this->logger->info('Product sent to Gubee API');
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            throw $e;
        }
        $this->logger->info('Product sent to Gubee API');
        return self::SUCCESS;
    }
}
