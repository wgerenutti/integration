<?php

declare(strict_types=1);

namespace Gubee\Integration\Console\Command\Catalog\Product\Stock;

use Exception;
use Gubee\Integration\Console\Command\AbstractCommand;
use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Log;
use Gubee\Integration\Service\Model\Product\Stock;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\App\ObjectManager;

class DesativateCommand extends AbstractCommand
{
    protected $stock;

    public function __construct(
        ProductInterface $product,
        Config $config,
        Log $logger,
        ?string $name = null
    ) {
        $this->stock = ObjectManager::getInstance()->create(
            Stock::class,
            [
                'product' => $product,
            ]
        );
        parent::__construct($config, $logger, $name);
    }

    protected function configure()
    {
        $this->setName('catalog:product:stock:desativate');
        $this->setDescription('Send product stock to Gubee API');
        parent::configure();
    }

    protected function _execute(): int
    {
        $this->logger->info('Disabling product stock to Gubee API');
        try {
            $this->stock->desativeStock();
            $this->logger->info('Product stock sent to Gubee API');
            return self::SUCCESS;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            throw $e;
        }
    }
}
