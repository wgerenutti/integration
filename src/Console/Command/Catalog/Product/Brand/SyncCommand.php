<?php

declare(strict_types=1);

namespace Gubee\Integration\Console\Command\Catalog\Product\Brand;

use Exception;
use Gubee\Integration\Console\Command\AbstractCommand;
use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Log;
use Gubee\Integration\Service\Model\Product\Brand;
use Magento\Eav\Model\ResourceModel\Entity\Attribute\CollectionFactory;
use Magento\Framework\App\ObjectManager;
use Psr\Log\LoggerInterface;

use function implode;
use function sprintf;

use const PHP_EOL;

class SyncCommand extends AbstractCommand
{
    protected $eavCollection;

    /**
     * @param Attribute         $config
     * @param LoggerInterface   $logger
     */
    public function __construct(
        CollectionFactory $eavCollection,
        Config $config,
        Log $logger,
        ?string $name = null
    ) {
        $this->eavCollection = $eavCollection;
        parent::__construct($config, $logger, $name);
    }

    public function configure()
    {
        $this->setDescription("Sync all brands to Gubee API");
    }

    protected function _execute(): int
    {
        if (! $this->getConfig()->getAttributeConfig()->getBrand()) {
            $this->getOutput()->writeln(
                sprintf(
                    "<error>Brand attribute is not configured, please before sync, configure the module.</error>"
                )
            );
            return self::FAILURE;
        }
        $exceptionChain = [];
        // check if brand attribute is a select
        $brandAttribute = $this->getAttribute();
        if ($brandAttribute->getFrontendInput() !== 'select') {
            return self::SUCCESS;
        }

        foreach ($this->getAttributeOptions() ?: [] as $option) {
            [$label, $value] = [$option['label'], $option['value']];
            if (! $value || ! $label) {
                continue;
            }
            $this->getOutput()->writeln(
                sprintf("<info>Syncing brand '%s'.</info>", $label)
            );
            $brand = ObjectManager::getInstance()->create(
                Brand::class
            );
            $brand->setId($value);
            $brand->setName($label);
            try {
                $brand->save();
                $this->getOutput()->writeln(
                    sprintf("<info>Brand</info> '%s' <info>synced.</info>", $label)
                );
            } catch (Exception $e) {
                $this->getOutput()->writeln(
                    sprintf(
                        "<error>Brand '%s' not synced. Error: %s</error>",
                        $label,
                        $e->getMessage()
                    )
                );
                $exceptionChain[] = $e->getMessage();
            }
        }
        if ($exceptionChain) {
            throw new Exception(
                sprintf(
                    "Error syncing brands: '%s'" . PHP_EOL,
                    implode("', '", $exceptionChain)
                )
            );
        }

        return self::SUCCESS;
    }

    /**
     * @return mixed
     */
    public function getAttributeOptions()
    {
        return $this->getAttribute()->getSource()
            ? $this->getAttribute()->getSource()->getAllOptions()
            : [];
    }

    /**
     * @return mixed
     */
    public function getAttribute()
    {
        $brand = $this->getEavCollection()->create()
            ->addFieldToFilter('attribute_code', $this->getConfig()
                ->getAttributeConfig()
                ->getBrand())->getFirstItem();
        if (! $brand->getId()) {
            throw new Exception(
                sprintf(
                    "Attribute '%s' not found. Validate the module configuration.",
                    $this->getConfig()->getBrand()
                )
            );
        }

        return $brand;
    }

    public function getEavCollection(): CollectionFactory
    {
        return $this->eavCollection;
    }

    /**
     * @return SyncCommand
     */
    public function setEavCollection(CollectionFactory $eavCollection): self
    {
        $this->eavCollection = $eavCollection;
        return $this;
    }
}
