<?php

declare(strict_types=1);

namespace Gubee\Integration\Console\Command\Catalog\Product;

use Exception;
use Gubee\Integration\Console\Command\AbstractCommand;
use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Log;
use Gubee\Integration\Service\Model\Product;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\App\ObjectManager;

class SendCommand extends AbstractCommand
{
    protected $product;

    public function __construct(
        ProductInterface $product,
        Config $config,
        Log $logger,
        ?string $name = null
    ) {
        $this->product = ObjectManager::getInstance()->create(
            Product::class,
            [
                'product' => $product,
            ]
        );
        parent::__construct($config, $logger, $name);
    }

    protected function configure()
    {
        $this->setName('catalog:product:send');
        $this->setDescription('Send product to Gubee API');
        parent::configure();
    }

    protected function _execute(): int
    {
        $this->logger->info('Sending product to Gubee API');
        try {
            $this->product->save();
            $this->logger->info('Product sent to Gubee API');
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            throw $e;
        }
        $this->logger->info('Product sent to Gubee API');
        return self::SUCCESS;
    }
}
