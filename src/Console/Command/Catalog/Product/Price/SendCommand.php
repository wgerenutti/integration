<?php

declare(strict_types=1);

namespace Gubee\Integration\Console\Command\Catalog\Product\Price;

use Exception;
use Gubee\Integration\Console\Command\AbstractCommand;
use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Log;
use Gubee\Integration\Service\Model\Product\Price;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Framework\App\ObjectManager;

class SendCommand extends AbstractCommand
{
    protected $price;

    public function __construct(
        ProductInterface $product,
        Config $config,
        Log $logger,
        ?string $name = null
    ) {
        $this->price = ObjectManager::getInstance()->create(
            Price::class,
            [
                'product' => $product,
            ]
        );
        parent::__construct($config, $logger, $name);
    }

    protected function configure()
    {
        $this->setName('catalog:product:price:send');
        $this->setDescription('Send product price to Gubee API');
        parent::configure();
    }

    protected function _execute(): int
    {
        $this->logger->info('Sending product to Gubee API');
        try {
            $this->price->save();
            $this->logger->info('Product sent to Gubee API');
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            throw $e;
        }
        $this->logger->info('Product sent to Gubee API');
        return self::SUCCESS;
    }
}
