<?php

declare(strict_types=1);

namespace Gubee\Integration\Console\Command\Catalog\Product\Attribute;

use Exception;
use Gubee\Integration\Console\Command\AbstractCommand;
use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Log;
use Gubee\Integration\Service\Model\Product\Attribute;
use Magento\Catalog\Api\Data\EavAttributeInterface;
use Magento\Framework\App\ObjectManager;

class SendCommand extends AbstractCommand
{
    protected $attribute;

    public function __construct(
        EavAttributeInterface $attribute,
        Config $config,
        Log $logger,
        ?string $name = null
    ) {
        $this->attribute = ObjectManager::getInstance()->create(
            Attribute::class,
            [
                'attribute' => $attribute,
            ]
        );
        parent::__construct($config, $logger, $name);
    }

    protected function configure()
    {
        $this->setName('catalog:product:attribute:send');
        $this->setDescription('Send product attribute to Gubee API');
        parent::configure();
    }

    protected function _execute(): int
    {
        $this->logger->info('Sending product attribute to Gubee API');
        try {
            $this->attribute->save();
            $this->logger->info('Product attribute sent to Gubee API');
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            return self::FAILURE;
        }
        $this->logger->info('Product attribute sent to Gubee API');
        throw $e;
    }
}
