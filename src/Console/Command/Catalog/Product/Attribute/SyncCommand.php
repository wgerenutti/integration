<?php

declare(strict_types=1);

namespace Gubee\Integration\Console\Command\Catalog\Product\Attribute;

use Exception;
use Gubee\Integration\Console\Command\AbstractCommand;
use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Config\Attribute;
use Gubee\Integration\Helper\Log;
use Gubee\Integration\Model\ResourceModel\Attribute\Collection;
use Gubee\SDK\Api\Product\AttributeApi;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Data\Collection\AbstractDb;
use Psr\Log\LoggerInterface;

use function sizeof;
use function sprintf;

class SyncCommand extends AbstractCommand
{
    protected $attributeCollection;
    /**
     * @param Collection $attributeCollectionFactory
     * @param Attribute         $config
     * @param LoggerInterface   $logger
     */
    public function __construct(
        Collection $attributeCollection,
        Config $config,
        Log $logger,
        ?string $name = null
    ) {
        $this->attributeCollection = $attributeCollection;
        parent::__construct($config, $logger, $name);
    }

    protected function configure()
    {
        $this->setName('catalog:product:attribute:sync');
        $this->setDescription('Sync product attribute from Gubee API');
        parent::configure();
    }

    protected function _execute(): int
    {
        $this->logger->info('Syncing product attribute from Gubee API');
        try {
            $attributes = [];
            $this->logger->info(
                sprintf(
                    "Found '%d' product attributes",
                    $this->getCollection()->getSize()
                )
            );
            foreach ($this->getCollection() as $attribute) {
                $attribute    = ObjectManager::getInstance()->create(
                    \Gubee\Integration\Service\Model\Product\Attribute::class,
                    [
                        'attribute' => $attribute,
                    ]
                );
                $attributes[] = $attribute;
            }
            if (sizeof($attributes) < 1) {
                return self::FAILURE;
            }

            $attributeApi = ObjectManager::getInstance()->create(
                AttributeApi::class
            );

            $attributeApi->bulkUpdate($attributes);
            $this->logger->info('Product attribute synced from Gubee API');
            return self::SUCCESS;
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
            throw $e;
        }
    }

    /**
     * @return mixed
     */
    public function getCollection(): AbstractDb
    {
        return $this->attributeCollection->getCollection();
    }
}
