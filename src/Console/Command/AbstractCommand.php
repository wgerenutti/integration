<?php

declare(strict_types=1);

namespace Gubee\Integration\Console\Command;

use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Log;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use function sprintf;

abstract class AbstractCommand extends Command
{
    const SUCCESS = 0;
    const FAILURE = 1;

    protected OutputInterface $output;
    protected InputInterface $input;

    protected Config $config;
    protected Log $logger;

    /**
     * @param string|null $name The name of the command; passing null means it must be set in configure()
     * @throws LogicException When the command name is empty
     */
    public function __construct(
        Config $config,
        Log $logger,
        ?string $name = null
    ) {
        $this->setConfig($config);
        $this->setLogger($logger);
        parent::__construct($name);
    }

    /**
     * Sets the name of the command.
     *
     * This method can set both the namespace and the name if
     * you separate them by a colon (:)
     *
     *     $command->setName('foo:bar');
     *
     * @param string $name The command name
     * @return $this
     * @throws InvalidArgumentException When the name is invalid
     */
    public function setName($name)
    {
        return parent::setName(
            sprintf(
                "gubee:%s",
                $name
            )
        );
    }

    /**
     * Executes the current command.
     *
     * This method is not abstract because you can use this class
     * as a concrete class. In this case, instead of defining the
     * execute() method, you set the code to execute by passing
     * a Closure to the setCode() method.
     *
     * @see setCode()
     *
     * @return int 0 if everything went fine, or an exit code
     * @throws LogicException When this abstract method is not implemented
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->getLogger()->info(
            sprintf(
                "Executing command '%s'",
                $this->getName()
            )
        );
        $this->setInput($input)
            ->setOutput($output);
        $result = $this->_execute();
        $this->getLogger()->info(
            sprintf(
                "Command '%s' executed with result '%s'",
                $this->getName(),
                $result
            )
        );
        return $result;
    }

    abstract protected function _execute(): int;

    public function getOutput(): OutputInterface
    {
        return $this->output;
    }

    public function setOutput(OutputInterface $output): self
    {
        $this->output = $output;
        return $this;
    }

    public function getInput(): InputInterface
    {
        return $this->input;
    }

    public function setInput(InputInterface $input): self
    {
        $this->input = $input;
        return $this;
    }

    public function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    public function setLogger(LoggerInterface $logger): self
    {
        $this->logger = $logger;
        return $this;
    }

    public function getConfig(): Config
    {
        return $this->config;
    }

    public function setConfig(Config $config): self
    {
        $this->config = $config;
        return $this;
    }
}
