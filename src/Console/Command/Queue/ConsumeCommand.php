<?php

declare(strict_types=1);

namespace Gubee\Integration\Console\Command\Queue;

use Exception;
use Gubee\Integration\Api\Data\JobInterface;
use Gubee\Integration\Console\Command\AbstractCommand;
use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Log;
use Gubee\Integration\Model\ResourceModel\Job\Collection;
use Gubee\Integration\Model\ResourceModel\Job\CollectionFactory;
use Symfony\Component\Console\Helper\ProgressBar;

use function sprintf;

class ConsumeCommand extends AbstractCommand
{
    /** @var Collection */
    protected $queue;

    public function __construct(
        CollectionFactory $queueCollectionFactory,
        Config $config,
        Log $logger,
        ?string $name = null
    ) {
        $this->queue = $queueCollectionFactory->create();
        parent::__construct($config, $logger, $name);
    }

    protected function configure()
    {
        $this->setName('consume');
        $this->setDescription('Consume queue');
        parent::configure();
    }

    public function _execute(): int
    {
        $this->getLogger()->info('Start consuming queue');
        $queue = $this->getQueue();
        $this->getLogger()->info(
            sprintf(
                "Found '%d' jobs to process",
                $queue->getSize()
            )
        );
        $progressBar = new ProgressBar(
            $this->getOutput(),
            $queue->getSize()
        );
        foreach ($queue as $job) {
            $progressBar->setMessage(
                sprintf(
                    "Processing job '%d'",
                    $job->getId()
                )
            );
            $this->getLogger()->info('Processing job ' . $job->getId());
            try {
                $job->process();
                $this->getLogger()->info(
                    sprintf(
                        "Job '%d' processed successfully",
                        $job->getId()
                    )
                );
            } catch (Exception $e) {
                $this->getLogger()->error($e->getMessage());
            } finally {
                $progressBar->advance();
            }
        }
        $this->getLogger()->info('Finished consuming queue');
        return self::SUCCESS;
    }

    /**
     * @return Collection
     */
    public function getQueue()
    {
        $queue  = $this->queue->addFieldToFilter(
            JobInterface::ATTEMPTS,
            [
                'lt' => $this->getConfig()->getMaxAttempts() ?: 3,
            ]
        )->addFieldToFilter(
            JobInterface::STATUS,
            [
                'nin' => JobInterface::STATUS_DONE_LIST,
            ]
        )
            ->setPageSize(
                $this->getConfig()->getQueueSize() ?: 100
            );
        $select = $queue->getSelect();
        $select->order(
            JobInterface::JOB_ID,
            Collection::SORT_ORDER_ASC
        )->order(
            JobInterface::PRIORITY,
            Collection::SORT_ORDER_ASC
        );
        return $queue;
    }
}
