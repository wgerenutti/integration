<?php

declare(strict_types=1);

namespace Gubee\Integration\Observer\Catalog\Category\Save;

use Gubee\Integration\Model\Job;
use Gubee\Integration\Observer\AbstractObserver;
use Magento\Framework\Event\Observer;

use function json_encode;

class After extends AbstractObserver
{
    public function process(Observer $observer)
    {
        $category = $observer->getEvent()->getCategory();
        /**
         * @var Job $obj
         */
        $obj = $this->getJobFactory()->create();
        $obj->setCode('category:sync');
        $obj->setPayload(json_encode(['category_id' => $category->getId()]));
        $obj->setPriority(0);
        $obj->save();
    }
}
