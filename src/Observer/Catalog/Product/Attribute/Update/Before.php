<?php

declare(strict_types=1);

namespace Gubee\Integration\Observer\Catalog\Product\Attribute\Update;

use Gubee\Integration\Observer\AbstractObserver;
use Magento\Framework\Event\Observer;

use function json_encode;

class Before extends AbstractObserver
{
    public function process(Observer $observer)
    {
        $productIds    = $observer->getEvent()->getProductIds();
        $attributeData = $observer->getEvent()->getAttributesData();
        if (! isset($attributeData['gubee'])) {
            return;
        }

        foreach ($productIds as $productId) {
            $this->createQueueItem($productId);
        }
    }

    protected function canCreateJob()
    {
        return $this->getConfig()->isEnabled();
    }

    public function createQueueItem($productId)
    {
        if (! $this->canCreateJob()) {
            return $this;
        }
        /**
         * @var Job $obj
         */
        $obj = $this->getJobFactory()->create();
        $obj->setCode('catalog:product:send');
        $obj->setPayload(json_encode(['product_id' => $productId]));
        $obj->setPriority(10);
        $obj->save();
    }
}
