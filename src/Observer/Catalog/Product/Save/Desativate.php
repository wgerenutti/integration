<?php

declare(strict_types=1);

namespace Gubee\Integration\Observer\Catalog\Product\Save;

use Gubee\Integration\Model\Job;
use Gubee\Integration\Observer\Catalog\Product\ProductAbstract;
use Gubee\Integration\Service\Model\Product;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer;

use function json_encode;

class Desativate extends ProductAbstract
{
    public function process(Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $this->setProduct($product);
        $this->createJob();
        return $this->triggerForParent();
    }

    protected function createJob()
    {
        if (! $this->canCreateJob()) {
            return $this;
        }
        /**
         * @var Job $obj
         */
        $obj     = $this->getJobFactory()->create();
        $product = ObjectManager::getInstance()->create(
            Product::class,
            [
                'product' => $this->getProduct(),
            ]
        );

        if ($this->getProduct()->getStatus() == 2) {
            $obj->setCode('catalog:product:desativate');
            $obj->setPayload(json_encode(['product_id' => $product->getId()]));
            $obj->setPriority(99);
            $obj->save();
        }
    }

    protected function canCreateJob()
    {
        if (
            $this->getProduct()->dataHasChangedFor('status')
            &&
            $this->getRawAttributeValue('gubee')
        ) {
            return true;
        }
        if (! $this->getRawAttributeValue('gubee')) {
            return false;
        }

        return true;
    }
}
