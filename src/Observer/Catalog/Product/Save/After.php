<?php

declare(strict_types=1);

namespace Gubee\Integration\Observer\Catalog\Product\Save;

use Gubee\Integration\Model\Job;
use Gubee\Integration\Observer\Catalog\Product\ProductAbstract;
use Magento\Framework\Event\Observer;

use function json_encode;

class After extends ProductAbstract
{
    public function process(Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $this->setProduct($product);
        $this->createJob();
        return $this->triggerForParent();
    }

    protected function createJob()
    {
        if (! $this->canCreateJob()) {
            return $this;
        }
        /**
         * @var Job $obj
         */
        $obj = $this->getJobFactory()->create();
        if ($this->getProduct()->getStatus() != 2) {
            $obj->setCode('catalog:product:send');
            $obj->setPayload(json_encode(['product_id' => $this->getProduct()->getId()]));
            $obj->setPriority(10);
            $obj->save();
        }
    }
}
