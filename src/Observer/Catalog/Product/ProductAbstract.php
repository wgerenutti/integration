<?php

declare(strict_types=1);

namespace Gubee\Integration\Observer\Catalog\Product;

use Gubee\Integration\Observer\AbstractObserver;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;
use Magento\Framework\App\ObjectManager;

abstract class ProductAbstract extends AbstractObserver
{
    protected $product;

    protected function triggerForParent()
    {
        $originalProduct = $this->getProduct();
        $parentIds       = ObjectManager::getInstance()
            ->create(Configurable::class)
            ->getParentIdsByChild($this->getProduct()->getId());
        if (empty($parentIds)) {
            return $this;
        }
        foreach ($parentIds as $parentId) {
            $parent = ObjectManager::getInstance()
                ->create(ProductRepositoryInterface::class)
                ->getById($parentId);
            $this->setProduct($parent);
            $this->createJob();
        }
        $this->setProduct($originalProduct);
        return $this;
    }

    public function getRawAttributeValue($attributeCode)
    {
        return $this->getProduct()->getResource()
            ->getAttributeRawValue(
                $this->getProduct()->getId(),
                $attributeCode,
                $this->getProduct()->getStoreId()
            );
    }

    public function getAttributeValueLabel($attributeCode)
    {
        $attribute = $this->getProduct()->getResource()
            ->getAttribute($attributeCode);
        if (! $attribute) {
            return null;
        }
        $frontend = $attribute->getFrontend();
        if (! $frontend) {
            return null;
        }
        return $frontend->getValue(
            $this->getProduct()
        );
    }

    protected function canCreateJob()
    {
        if (
            $this->getProduct()->dataHasChangedFor('status')
            &&
            $this->getRawAttributeValue('gubee')
        ) {
            return true;
        }

        if (! $this->getRawAttributeValue('gubee_sync') && ! $this->getProduct()->dataHasChangedFor('gubee')) {
            return false;
        }
        if (! $this->getRawAttributeValue('gubee')) {
            return false;
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param mixed $product
     */
    public function setProduct(ProductInterface $product): self
    {
        $this->product = $product;
        return $this;
    }
}
