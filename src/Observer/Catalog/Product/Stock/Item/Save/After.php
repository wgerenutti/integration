<?php

declare(strict_types=1);

namespace Gubee\Integration\Observer\Catalog\Product\Stock\Item\Save;

use Gubee\Integration\Model\Job;
use Gubee\Integration\Observer\Catalog\Product\ProductAbstract;
use Magento\CatalogInventory\Api\StockRegistryInterface;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer;

use function json_decode;
use function json_encode;

class After extends ProductAbstract
{
    protected $hasChildChanged = false;
    public function process(Observer $observer)
    {
        $product = $observer->getProduct();
        if (!$product) {
            return $this;
        }

        $this->setProduct($product);
        if ($this->isChild() && $this->checkIfStockHasChanged($product)) {
            $this->hasChildChanged = true;
        }

        $this->createJob();
        return $this->triggerForParent();
    }

    protected function checkIfStockHasChanged($product)
    {
        $origJson = json_decode(json_encode($product->getOrigData()), true);
        if (isset($origJson['quantity_and_stock_status'])) {
            if (
                $origJson['quantity_and_stock_status']['qty'] == $product->getData('stock_data/qty')
                &&
                $origJson['quantity_and_stock_status']['is_in_stock'] == $product->getData('stock_data/is_in_stock')
            ) {
                return false;
            }
        }

        return true;
    }

    protected function isChild()
    {
        $originalProduct = $this->getProduct();
        $parentIds       = ObjectManager::getInstance()
            ->create(Configurable::class)
            ->getParentIdsByChild($this->getProduct()->getId());
        if (empty($parentIds)) {
            return false;
        }

        return true;
    }

    protected function canCreateJob($fromChild = false)
    {
        if (!$this->getRawAttributeValue('gubee')) {
            return false;
        }

        if ($this->hasChildChanged) {
            return true;
        }

        if ($this->checkIfStockHasChanged($this->getProduct())) {
            return true;
        }
        return false;
    }

    protected function createJob()
    {
        if (!$this->canCreateJob()) {
            return $this;
        }
        $obj = $this->getJobFactory()->create();

        $stock = $this->getProduct()->getStockData();
        if (!$stock) {
            $stock = ObjectManager::getInstance()->create(
                StockRegistryInterface::class
            )->getStockItem(
                $this->getProduct()->getId()
            );
            $stock = $stock->getData();
        }

        if (!$stock['is_in_stock']) {
            $obj->setCode('catalog:product:stock:desativate');
        } else {
            $obj->setCode('catalog:product:stock:send');
        }
        $obj->setPriority(99);
        /**
         * @var Job $obj
         */
        $obj->setPayload(json_encode(['product_id' => $this->getProduct()->getId()]));
        $obj->save();
    }
}
