<?php

declare(strict_types=1);

namespace Gubee\Integration\Observer\Catalog\Product\Price\Save;

use Gubee\Integration\Model\Job;
use Gubee\Integration\Observer\Catalog\Product\ProductAbstract;
use Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer;

use function json_encode;

class After extends ProductAbstract
{
    protected $hasChildChanged = false;

    public function process(Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $this->setProduct($product);
        if (
            $this->isChild() &&
            $this->getProduct()->dataHasChangedFor(
                $this->getConfig()->getAttributeConfig()->getPrice()
            )
        ) {
            $this->hasChildChanged = true;
        }

        $this->createJob();
        return $this->triggerForParent();
    }

    protected function canCreateJob()
    {
        if (! $this->getRawAttributeValue('gubee')) {
            return false;
        }
        if ($this->hasChildChanged) {
            return true;
        }
        if (
            $this->getProduct()->dataHasChangedFor(
                $this->getConfig()->getAttributeConfig()->getPrice()
            )
        ) {
            return true;
        }
        return false;
    }

    protected function isChild()
    {
        $originalProduct = $this->getProduct();
        $parentIds       = ObjectManager::getInstance()
            ->create(Configurable::class)
            ->getParentIdsByChild($this->getProduct()->getId());
        if (empty($parentIds)) {
            return false;
        }

        return true;
    }

    protected function createJob()
    {
        if (! $this->canCreateJob()) {
            return $this;
        }
        /**
         * @var Job $obj
         */
        $obj = $this->getJobFactory()->create();
        $obj->setCode('catalog:product:price:send');
        $obj->setPayload(json_encode(['product_id' => $this->getProduct()->getId()]));
        $obj->setPriority(2);
        $obj->save();
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        $product = $this->getProduct();
        if (
            $this->getConfig()
            && $this->getConfig()->getAttributeConfig()->getPrice()
            && $this->getConfig()->getAttributeConfig()->getPrice() != 'price'
        ) {
            // get attribute value
            $productResource = $product->getResource();
            $attribute       = $productResource->getAttribute($this->getConfig()->getAttributeConfig()->getPrice());
            $attributeValue  = $product->getData($this->getConfig()->getAttributeConfig()->getPrice());
            $attributeValue  = $attribute->getFrontend()->getValue($product);
            return $attributeValue;
        }

        return $product->getPrice();
    }
}
