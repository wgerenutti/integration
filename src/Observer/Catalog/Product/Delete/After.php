<?php

declare(strict_types=1);

namespace Gubee\Integration\Observer\Catalog\Product\Delete;

use Gubee\Integration\Observer\Catalog\Product\ProductAbstract;
use Gubee\Integration\Service\Model\Product;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer;

use function json_encode;

class After extends ProductAbstract
{
    public function process(Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $this->setProduct($product);
        if (! $this->getProduct()->getGubee()) {
            return false;
        }
        $this->createJob();
        return $this->triggerForParent();
    }

    protected function canCreateJob()
    {
        if (! $this->getProduct()->getGubee()) {
            return false;
        }

        return true;
    }

    protected function createJob()
    {
        if (! $this->canCreateJob()) {
            return $this;
        }
        /**
         * @var Job $obj
         */
        $obj     = $this->getJobFactory()->create();
        $product = ObjectManager::getInstance()->create(
            Product::class,
            [
                'product' => $this->getProduct(),
            ]
        );
        $obj->setCode('catalog:product:desativate');
        $obj->setPayload(json_encode(['product_id' => $product->getId()]));
        $obj->setPriority(99);
        $obj->save();
    }
}
