<?php

declare(strict_types=1);

namespace Gubee\Integration\Observer\Catalog\Attribute\Save;

use Gubee\Integration\Model\Job;
use Gubee\Integration\Observer\AbstractObserver;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Framework\Event\Observer;

class After extends AbstractObserver
{
    public function execute(Observer $observer)
    {
        $object = $observer->getObject();

        if (! $object instanceof Attribute) {
            return;
        }
        return parent::execute($observer);
    }

    /**
     * @return null
     */
    public function process(Observer $observer)
    {
        $object = $observer->getObject();
        if (! $object instanceof Attribute) {
            return;
        }
        /**
         * @var Job $obj
         */
        $obj = $this->getJobFactory()->create();
        $obj->setCode('attribute:sync');
        $obj->setPriority(10);
        $obj->save();
    }
}
