<?php

declare(strict_types=1);

namespace Gubee\Integration\Observer\Catalog\Attribute\Brand\Save;

use Gubee\Integration\Model\Job;
use Gubee\Integration\Observer\AbstractObserver;
use Magento\Catalog\Model\ResourceModel\Eav\Attribute;
use Magento\Framework\Event\Observer;

class After extends AbstractObserver
{
    public function execute(Observer $observer)
    {
        $object = $observer->getObject();
        if (! $object instanceof Attribute) {
            return;
        }
        return parent::execute($observer);
    }

    public function process(Observer $observer)
    {
        $attribute = $observer->getObject();
        if ($attribute->getAttributeCode() != $this->getConfig()->getAttributeConfig()->getBrand()) {
            return;
        }
        /**
         * @var Job $obj
         */
        $obj = $this->getJobFactory()->create();
        $obj->setCode('brand:sync');
        $obj->setPriority(10);
        $obj->save();
    }
}
