<?php

declare(strict_types=1);

namespace Gubee\Integration\Observer\Sales\Order\Save;

use Magento\Framework\Event\Observer;

class After extends \Gubee\Integration\Observer\Catalog\Product\Stock\Item\Save\After
{
    public function process(Observer $observer)
    {
        $order = $observer->getEvent()->getOrder();
        $items = $order->getAllItems();
        foreach ($items as $item) {
            $product = $item->getProduct();
            $this->setProduct($product);
            if ($this->isChild() && $this->checkIfStockHasChanged($product)) {
                $this->hasChildChanged = true;
            }

            $this->createJob();
            $this->triggerForParent();
        }
    }
}
