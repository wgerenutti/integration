<?php

declare(strict_types=1);

namespace Gubee\Integration\Observer;

use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Log;
use Gubee\Integration\Model\JobFactory;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

use function json_encode;
use function sprintf;

abstract class AbstractObserver implements ObserverInterface
{
    /** @var Config */
    protected $config;

    /** @var LoggerInterface */
    protected $logger;

    /** @var JobFactory */
    protected $jobFactory;

    public function __construct(
        Config $config,
        Log $logger,
        JobFactory $jobFactory
    ) {
        $this->jobFactory = $jobFactory;
        $this->config     = $config;
        $this->logger     = $logger;
    }

    abstract protected function process(Observer $observer);

    public function execute(Observer $observer)
    {
        if (! $this->isAllowed()) {
            return;
        }
        $this->getLogger()->debug(
            sprintf(
                "Observer '%s' is running",
                __METHOD__
            )
        );
        $result = $this->process($observer);
        $this->getLogger()->debug(
            sprintf(
                "Observer '%s' is finished",
                __METHOD__
            )
        );
        return $result;
    }

    public function createAJob($name, $data)
    {
        $obj = $this->getJobFactory()->create();
        $obj->setCode($name);
        $obj->setPayload(json_encode($data));
        $obj->save();
    }

    public function isAllowed()
    {
        return $this->getConfig()->isEnabled();
    }

    /**
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Config $config
     */
    public function setConfig($config): self
    {
        $this->config = $config;
        return $this;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger($logger): self
    {
        $this->logger = $logger;
        return $this;
    }

    /**
     * @return JobFactory
     */
    public function getJobFactory()
    {
        return $this->jobFactory;
    }

    public function setJobFactory(JobFactory $jobFactory): self
    {
        $this->jobFactory = $jobFactory;
        return $this;
    }
}
