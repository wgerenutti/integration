<?php

declare(strict_types=1);

namespace Gubee\Integration\Observer\Backend\Admin\System\Config;

use Exception;
use Gubee\Integration\Helper\Config;
use Gubee\Integration\Helper\Log;
use Gubee\Integration\Model\Job;
use Gubee\Integration\Model\JobFactory;
use Gubee\Integration\Observer\AbstractObserver;
use Gubee\Integration\Service\Client;
use Magento\Framework\App\Cache\Frontend\Pool;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Event\Observer;
use Psr\Log\LoggerInterface;

use function in_array;

class Changed extends AbstractObserver
{
    /** @var Client */
    protected $gubee;

    protected $logger;

    /**
     * @param Gubee           $gubee
     * @param LoggerInterface $logger
     */
    public function __construct(
        Client $gubee,
        Config $config,
        Log $logger,
        JobFactory $jobFactory
    ) {
        $this->gubee = $gubee;
        parent::__construct($config, $logger, $jobFactory);
    }

    protected function process(Observer $observer)
    {
        $this->getLogger()->info('Gubee config changed');
        $changedPaths = $observer->getData('changed_paths');
        if (
            in_array('gubee/general/active', $changedPaths)
        ) {
            $this->getLogger()->info('Gubee integration has changed active config');
            $this->clearAdminMenuCache();
        }

        try {
            if (
                in_array('gubee/general/api_key', $changedPaths)
                ||
                $this->getGubee()->isTokenTimeouted()
            ) {
                $this->getConfig()->setApiToken('');
                $this->getConfig()->setApiTimeout('');
                $this->getConfig()->unsetData('api_key');
                $this->getConfig()->save();
                $this->getConfig()->load();
                $this->getLogger()->info('API key changed, renewing token');
                $this->getGubee()->renewToken();
                $this->getLogger()->info('Token renewed');
            }
        } catch (Exception $e) {
            $this->getLogger()->error($e->getMessage());
        } finally {
            $queue = ObjectManager::getInstance()->create(
                Job::class
            )->setCode('attribute:sync');
            $queue->save();
            $queue = ObjectManager::getInstance()->create(
                Job::class
            )->setCode('category:sync');
            $queue->save();
            $queue = ObjectManager::getInstance()->create(
                Job::class
            )->setCode('brand:sync');
            $queue->save();
        }

        return $this;
    }

    private function clearAdminMenuCache()
    {
        $cacheTypeList     = ObjectManager::getInstance()->create(
            TypeListInterface::class
        );
        $cacheFrontendPool = ObjectManager::getInstance()->create(
            Pool::class
        );
        $types             = [
            'config',
            'layout',
            'full_page',
            'translate',
        ];
        foreach ($types as $type) {
            $cacheTypeList->cleanType($type);
        }
        foreach ($cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
    }

    /**
     * @return Client
     */
    public function getGubee()
    {
        return $this->gubee;
    }

    /**
     * @param Client $gubee
     */
    public function setGubee($gubee): self
    {
        $this->gubee = $gubee;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param mixed $logger
     */
    public function setLogger($logger): self
    {
        $this->logger = $logger;
        return $this;
    }
}
