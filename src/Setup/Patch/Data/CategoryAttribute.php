<?php

declare(strict_types=1);

namespace Gubee\Integration\Setup\Patch\Data;

use Magento\Catalog\Model\Category;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;

class CategoryAttribute implements DataPatchInterface, PatchRevertableInterface
{
    /** @var EavSetupFactory */
    private $eavSetupFactory;

    /** @var ModuleDataSetupInterface */
    private $moduleDataSetup;

    /**
     * Constructor
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup
            ->getConnection()
            ->startSetup();
        /**
         * @var EavSetup $eavSetup
         */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $eavSetup->addAttribute(
            Category::ENTITY,
            'gubee',
            [
                'type'     => 'int',
                'group'    => 'Gubee',
                'label'    => 'Active sync with Gubee',
                'comment'  => 'By enabling this option, the category will be synchronized with Gubee, every change made in the category will be sent to Gubee.',
                'input'    => 'boolean',
                'source'   => Boolean::class,
                'visible'  => true,
                'default'  => '0',
                'required' => false,
                'global'   => ScopedAttributeInterface::SCOPE_STORE,
            ]
        );
        $this->moduleDataSetup
            ->getConnection()
            ->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        /**
         * @var EavSetup $eavSetup
         */
        $eavSetup = $this->eavSetupFactory->create(
            [
                'setup' => $this->moduleDataSetup,
            ]
        );
        $eavSetup->removeAttribute(
            Category::ENTITY,
            'gubee'
        );

        $this->moduleDataSetup->getConnection()
            ->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies()
    {
        return [];
    }
}
