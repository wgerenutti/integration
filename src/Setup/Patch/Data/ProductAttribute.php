<?php

declare(strict_types=1);

namespace Gubee\Integration\Setup\Patch\Data;

use Gubee\Integration\Model\Product\Attribute\Backend\Source\HandlingTime;
use Gubee\Integration\Model\Product\Attribute\Backend\Source\Origin;
use Gubee\SDK\DataObject\Product\Attribute\Dimension\UnitTime;
use Magento\Catalog\Model\Product;
use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Magento\Eav\Model\Entity\Attribute\Source\Table;
use Magento\Eav\Setup\EavSetup;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Framework\Setup\Patch\PatchRevertableInterface;

use function array_merge;
use function sprintf;

class ProductAttribute implements DataPatchInterface, PatchRevertableInterface
{
    /** @var ModuleDataSetupInterface */
    private $moduleDataSetup;
    /** @var EavSetupFactory */
    private $eavSetupFactory;

    /** @var array */
    protected $attributes = [
        'gubee'                         => [
            'type'                    => 'int',
            'label'                   => 'Send product to Gubee',
            'user_defined'            => false,
            'is_visible'              => 0,
            'input'                   => 'boolean',
            'source'                  => Boolean::class,
            'used_in_product_listing' => true,
        ],
        'gubee_sync'                    => [
            'type'    => 'int',
            'input'   => 'boolean',
            'label'   => 'Keep synced with Gubee',
            'source'  => Boolean::class,
            'comment' => 'If checked, this product will automatically be synced with Gubee on every save.',
        ],
        'gubee_brand'                   => [
            'type'   => 'int',
            'label'  => 'Brand',
            'input'  => 'select',
            'source' => Table::class,
        ],
        'gubee_origin'                  => [
            'type'   => 'varchar',
            'label'  => 'Origin',
            'input'  => 'select',
            'source' => Origin::class,
        ],
        'gubee_nbm'                     => [
            'type'  => 'varchar',
            'label' => 'NBM',
        ],
        'gubee_ean'                     => [
            'type'  => 'varchar',
            'label' => 'EAN',
        ],
        'gubee_width'                   => [
            'type'  => 'decimal',
            'label' => 'Width',
        ],
        'gubee_height'                  => [
            'type'  => 'decimal',
            'label' => 'Height',
        ],
        'gubee_depth'                   => [
            'type'  => 'decimal',
            'label' => 'Depth',
        ],
        'gubee_handling_time'           => [
            'type'  => 'int',
            'label' => 'Handling Time',
        ],
        'gubee_handling_time_unit'      => [
            'type'   => 'varchar',
            'label'  => 'Handling Time Unit',
            'input'  => 'select',
            'source' => HandlingTime::class,
        ],
        'gubee_warranty_time'           => [
            'type'  => 'int',
            'label' => 'Warranty Time',
        ],
        'gubee_warranty_time_unit'      => [
            'type'   => 'varchar',
            'label'  => 'Warranty Time Unit',
            'input'  => 'select',
            'source' => HandlingTime::class,
        ],
        'gubee_price'                   => [
            'type'  => 'decimal',
            'label' => 'Price',
            'input' => 'price',
        ],
        'gubee_cross_docking_time'      => [
            'type'  => 'int',
            'label' => 'Cross Docking Time',
        ],
        'gubee_cross_docking_time_unit' => [
            'type'    => 'varchar',
            'label'   => 'Cross Docking Time Unit',
            'input'   => 'select',
            'source'  => HandlingTime::class,
            'default' => UnitTime::DAYS,
            'note'    => 'Time to prepare the product to be shipped after the order is placed, if none is set days will be used by default.',
        ],
    ];

    /**
     * Constructor
     */
    public function __construct(
        ModuleDataSetupInterface $moduleDataSetup,
        EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        /**
         * @var EavSetup $eavSetup
         */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        $sort     = 1;
        foreach ($this->attributes as $attrKey => $attrValue) {
            $this->addAttribute($eavSetup, $attrKey, $attrValue, $sort);
        }
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @param  EavSetup $eavSetup
     * @param  string   $attrKey
     * @param  array    $attrValue
     * @param  int      $sort
     * @return void
     */
    private function addAttribute($eavSetup, $attrKey, $attrValue, &$sort)
    {
        /**
         * Check if attribute exists
         */
        if ($eavSetup->getAttributeId(Product::ENTITY, $attrKey)) {
            $sort++;
            return;
        }

        $eavSetup->addAttribute(
            Product::ENTITY,
            $attrKey,
            array_merge(
                [
                    'global'                  => ScopedAttributeInterface::SCOPE_STORE,
                    'input'                   => 'text',
                    'default'                 => null,
                    'visible'                 => true,
                    'required'                => 'false',
                    'user_defined'            => true,
                    'searchable'              => false,
                    'filterable'              => false,
                    'comparable'              => false,
                    'visible_on_front'        => false,
                    'unique'                  => false,
                    'group'                   => "Gubee",
                    'sort_order'              => $sort * 10,
                    'used_in_product_listing' => false,
                    'is_used_in_grid'         => true,
                    'is_visible_in_grid'      => false,
                    'is_filterable_in_grid'   => false,
                ],
                $attrValue,
                [
                    'label' => $this->getAttributeLabel($attrValue['label']),
                ]
            )
        );
        $sort++;
    }

    /**
     * {@inheritdoc}
     *
     * @return void
     */

    public function revert()
    {
        $this->moduleDataSetup->getConnection()->startSetup();
        /**
         * @var EavSetup $eavSetup
         */
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);
        foreach ($this->attributes as $attrKey => $attrValue) {
            $eavSetup->removeAttribute(
                Product::ENTITY,
                $attrKey
            );
        }
        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * {@inheritdoc}
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public static function getDependencies(): array
    {
        return [];
    }

    public function getAttributeLabel(string $label): string
    {
        if ($label == 'gubee') {
            return 'Gubee';
        }
        return sprintf(
            "Gubee > %s",
            $label
        );
    }
}
