<?php

declare(strict_types=1);

namespace Gubee\Integration\Helper\Config;

use Gubee\Integration\Helper\AbstractConfig;
use Gubee\SDK\DataObject\Product\Attribute\Dimension\Weight;

use function array_filter;
use function explode;

class Attribute extends AbstractConfig
{
    protected $configPath = 'gubee/attributes';

    public function getWeightUnit(): string
    {
        switch ($this->scopeConfig->getValue('general/locale/weight_unit')) {
            case 'lbs':
                return Weight::POUND;
            case 'kg':
                return Weight::KILOGRAM;
        }
        return Weight::KILOGRAM;
    }

    public function getBlacklist()
    {
        $blacklist = parent::getBlacklist();
        return array_filter(
            explode(
                ',',
                $blacklist ?: ''
            )
        );
    }
}
