<?php

declare(strict_types=1);

namespace Gubee\Integration\Helper;

use BadMethodCallException;
use Magento\Framework\App\Cache\Frontend\Pool;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\DataObject;

use function sprintf;

use const DIRECTORY_SEPARATOR;

abstract class AbstractConfig extends AbstractHelper
{
    protected $configPath;

    protected DataObject $config;

    protected WriterInterface $configWriter;

    protected TypeListInterface $cacheTypeList;

    protected Pool $pool;

    public function __construct(
        Context $context,
        WriterInterface $configWriter,
        TypeListInterface $cacheTypeList,
        Pool $pool
    ) {
        parent::__construct($context);
        $this->configWriter  = $configWriter;
        $this->cacheTypeList = $cacheTypeList;
        $this->pool          = $pool;
        $this->config        = new DataObject(
            $this->scopeConfig->getValue($this->getConfigPath()) ?: []
        );
    }

    public function save(): self
    {
        if (
            $this->getConfig()->getOrigData() === $this->getConfig()->getData()
        ) {
            return $this;
        }
        foreach ($this->getConfig()->getData() as $key => $value) {
            $this->getConfigWriter()->save(
                $this->getConfigPath() . DIRECTORY_SEPARATOR . $key,
                $value
            );
        }
        $this->getCacheTypeList()->cleanType('config');
        $this->getPool()->get('config')->clean();

        return $this;
    }

    /**
     * @param string $name
     * @param array  $arguments
     */
    public function __call(
        $name,
        $arguments
    ) {
        return $this->config->$name(...$arguments);
    }

    public function getConfig(): DataObject
    {
        return $this->config;
    }

    public function getConfigPath(): string
    {
        if (! $this->configPath) {
            throw new BadMethodCallException(
                sprintf(
                    'Config path is not defined on %s',
                    static::class
                )
            );
        }
        return $this->configPath;
    }

    public function getConfigWriter(): WriterInterface
    {
        return $this->configWriter;
    }

    public function setConfigPath(string $configPath): self
    {
        $this->configPath = $configPath;
        return $this;
    }

    public function getCacheTypeList(): TypeListInterface
    {
        return $this->cacheTypeList;
    }

    public function getPool(): Pool
    {
        return $this->pool;
    }
}
