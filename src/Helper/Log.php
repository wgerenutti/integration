<?php

declare(strict_types=1);

namespace Gubee\Integration\Helper;

use Gubee\Integration\Model\Log as LogModel;
use Gubee\Integration\Model\LogFactory;
use Gubee\Integration\Model\ResourceModel\Log as LogResourceModel;
use Psr\Log\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Throwable;

use function array_merge_recursive;
use function getmypid;
use function in_array;
use function json_encode;
use function memory_get_peak_usage;
use function memory_get_usage;

class Log implements LoggerInterface
{
    /** @var Gubee\Integration\Model\LogFactory */
    protected $logFactory;

    /** @var Gubee\Integration\Model\ResourceModel\Log */
    protected $logResourceModel;

    /** @var LoggerInterface */
    protected $logger;

    protected $config;

    public function __construct(
        Config $config,
        LogFactory $logFactory,
        LogResourceModel $logResourceModel,
        LoggerInterface $logger
    ) {
        $this->config           = $config;
        $this->logFactory       = $logFactory;
        $this->logResourceModel = $logResourceModel;
        $this->logger           = $logger;
    }

    /**
     * System is unusable.
     *
     * @param string  $message
     * @param mixed[] $context
     * @return void
     */
    public function emergency($message, array $context = [])
    {
        $this->log(
            LogModel::LEVEL_EMERGENCY,
            $message,
            $context
        );
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string  $message
     * @param mixed[] $context
     * @return void
     */
    public function alert($message, array $context = [])
    {
        $this->log(
            LogModel::LEVEL_ALERT,
            $message,
            $context
        );
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string  $message
     * @param mixed[] $context
     * @return void
     */
    public function critical($message, array $context = [])
    {
        $this->log(
            LogModel::LEVEL_CRITICAL,
            $message,
            $context
        );
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string  $message
     * @param mixed[] $context
     * @return void
     */
    public function error($message, array $context = [])
    {
        $this->log(
            LogModel::LEVEL_ERROR,
            $message,
            $context
        );
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string  $message
     * @param mixed[] $context
     * @return void
     */
    public function warning($message, array $context = [])
    {
        $this->log(
            LogModel::LEVEL_WARNING,
            $message,
            $context
        );
    }

    /**
     * Normal but significant events.
     *
     * @param string  $message
     * @param mixed[] $context
     * @return void
     */
    public function notice($message, array $context = [])
    {
        $this->log(
            LogModel::LEVEL_NOTICE,
            $message,
            $context
        );
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string  $message
     * @param mixed[] $context
     * @return void
     */
    public function info($message, array $context = [])
    {
        $this->log(
            LogModel::LEVEL_INFO,
            $message,
            $context
        );
    }

    /**
     * Detailed debug information.
     *
     * @param string  $message
     * @param mixed[] $context
     * @return void
     */
    public function debug($message, array $context = [])
    {
        $this->log(
            LogModel::LEVEL_DEBUG,
            $message,
            $context
        );
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed   $level
     * @param string  $message
     * @param mixed[] $context
     * @return void
     * @throws InvalidArgumentException
     */
    public function log($level, $message, array $context = [])
    {
        if (
            ! in_array(
                $level,
                $this->getConfig()->getLogLevel()
            )
        ) {
            return;
        }

        try {
            $context = array_merge_recursive(
                [
                    'memory_usage'      => memory_get_usage(true),
                    'memory_peak_usage' => memory_get_peak_usage(true),
                    'uri'               => $_SERVER['REQUEST_URI'] ?? '',
                    'method'            => $_SERVER['REQUEST_METHOD'] ?? '',
                    'ip'                => $_SERVER['REMOTE_ADDR'] ?? '',
                    'user_agent'        => $_SERVER['HTTP_USER_AGENT'] ?? '',
                    'pid'               => getmypid(),
                ],
                $context
            );

            $log = $this->getLogFactory()->create();
            $log->setLevel($level);
            $log->setMessage($message);
            $log->setContext(json_encode($context));
            $this->getLogResourceModel()->save($log);
        } catch (Throwable $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * @return Gubee\Integration\Model\LogFactory
     */
    public function getLogFactory()
    {
        return $this->logFactory;
    }

    /**
     * @param Gubee\Integration\Model\LogFactory $logFactory
     */
    public function setLogFactory($logFactory): self
    {
        $this->logFactory = $logFactory;
        return $this;
    }

    /**
     * @return Gubee\Integration\Model\ResourceModel\Log
     */
    public function getLogResourceModel()
    {
        return $this->logResourceModel;
    }

    /**
     * @param Gubee\Integration\Model\ResourceModel\Log $logResourceModel
     */
    public function setLogResourceModel($logResourceModel): self
    {
        $this->logResourceModel = $logResourceModel;
        return $this;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param LoggerInterface $logger
     */
    public function setLogger($logger): self
    {
        $this->logger = $logger;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param mixed $config
     */
    public function setConfig($config): self
    {
        $this->config = $config;
        return $this;
    }
}
