<?php

declare(strict_types=1);

namespace Gubee\Integration\Helper;

use Gubee\Integration\Helper\Config\Attribute;
use Magento\Framework\App\ObjectManager;
use Psr\Log\LogLevel;

use function array_merge;
use function explode;

class Config extends AbstractConfig
{
    /** @var string */
    protected $configPath = 'gubee/general';

    public function isEnabled()
    {
        return $this->getConfig()->getData('active') ?: false;
    }

    public function getMaxBackoffAttempts()
    {
        return $this->getConfig()->getData('max_backoff_attempts') ?: 0;
    }

    public function getAttributeConfig()
    {
        return ObjectManager::getInstance()
            ->get(Attribute::class);
    }

    public function getLogLevel()
    {
        return array_merge(
            [LogLevel::ERROR],
            explode(',', $this->getConfig()->getData('log_level') ?: '') ?: []
        );
    }
}
